﻿using BackOffice.Models;
using BackOffice.Service;
using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOffice.Controllers
{
    public class CurseController : Controller
    {
        CurseService curseService = new CurseService();
        CurseModel curseModel = new CurseModel();
        TechnologyService technologyService = new TechnologyService();
        SessionController sessionController = new SessionController();

        public ActionResult index()
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                curseModel.listCurse = curseService.listCurseService();
                curseModel.listTechnology = technologyService.listTechnologyService();
                curseModel.userSession = userSession;
                return manageSession(userSession, curseModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        public ActionResult formCreateCurse()
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                curseModel.validateCurse = TempData["validateCurse"] as ValidateCurse;
                curseModel.listTechnology = technologyService.listTechnologyService();
                curseModel.userSession = userSession;
                return manageSession(userSession, curseModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public JsonResult validateCurse(string name, string description, string requirements, string content, string file, int? idTec, int active)
        {
            ValidateCurse validateCurse = curseService.validateCurseService(name, description, requirements, content, idTec, file, active);
            return Json(validateCurse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult createCurse(string name, string description, string requirements, string content, HttpPostedFileBase file, int idTec, int active)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                this.curseService.createCurseService(name, description, requirements, content, file, idTec, active, userSession.Id);
                return RedirectToAction("index", "Curse");
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpGet]
        public ActionResult formUpdateCurse(int id)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                curseModel.validateCurse = TempData["validateCurse"] as ValidateCurse;
                curseModel.listTechnology = technologyService.listTechnologyService();
                curseModel.curse = curseService.findCurseService(id);
                curseModel.userSession = userSession;
                return manageSession(userSession, curseModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public JsonResult updateValidateCurse(string name, string description, string requirements, string content, string file, int? idTec, int active)
        {
            ValidateCurse validateCurse = curseService.validateCurseService(name, description, requirements, content, idTec, file, active, true);
            return Json(validateCurse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateCurse(int id, string name, string description, string requirements, string content, HttpPostedFileBase file, int idTec, int active)
        {
            if (null != Session["user"] as User)
            {
                curseService.updateCurseService(id, description, name, requirements, content, idTec, file, active);
                return RedirectToAction("index", "Curse");
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpGet]
        public ActionResult detailCurse(int id)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                curseModel.curse = curseService.findCurseService(id);
                curseModel.userSession = userSession;
                return manageSession(userSession, curseModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpGet]
        public ActionResult filterCurse(int active,int idTec)
        {
            if (null != Session["user"] as User)
            {
                curseModel.listCurse = curseService.filterCurseService(active, idTec);
                return View(curseModel);
            }
            else {
                return RedirectToAction("index", "Login");
            }
        }

        public ActionResult manageSession(User userSession, CurseModel curseModel)
        {
            if (userSession.Rol.Id == 1)
            {
                return View(curseModel);
            }
            else
            {
                return RedirectToAction("index", "Teacher");
            }
        }
    }
}