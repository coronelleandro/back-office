﻿using BackOffice.Models;
using BackOffice.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BackOffice.Validate;
using System.Web.Mvc;
using BackOffice.Repository;

namespace BackOffice.Controllers
{
    public class CurseItemController : Controller
    {
        private CurseItemModel curseItemModel = new CurseItemModel();
        private CurseItemService curseItemService = new CurseItemService();
        private TeacherService teacherService = new TeacherService();
        private CurseService curseService = new CurseService();
        private CurseItemStateService curseItemStateService = new CurseItemStateService();
        private TechnologyService technologyService = new TechnologyService();
        private StudentService studentService = new StudentService();
        private BackOfficeEntities context = new BackOfficeEntities();

        public ActionResult index()
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                curseItemModel.listTechnologies = technologyService.listTechnologyService();
                curseItemModel.listCurseItems = curseItemService.listCurseItemsService();
                curseItemModel.listCurseItemState = curseItemStateService.listCurseItemStateService();
                curseItemModel.userSession = userSession;
                return manageSession(userSession, curseItemModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        public ActionResult formCreateCurseItem()
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                curseItemModel.validateCurseItem = TempData["validateCurseItem"] as ValidateCurseItem;
                curseItemModel.ListCurse = curseService.listCurseService();
                curseItemModel.listTeacher = teacherService.listTeacher();
                curseItemModel.listCurseItemState = curseItemStateService.listCurseItemStateService();
                curseItemModel.userSession = userSession;
                return manageSession(userSession, curseItemModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public JsonResult validateCurseItem(int idCurse, DateTime? dateStart, DateTime? dateFinish, DateTime? timeStart, DateTime? timeFinish, int idTeacher, int[] days, int active, int? studentsCapacity, decimal? price)
        {
            ValidateCurseItem validateCurseItem = curseItemService.validateCurseItemService(idCurse, dateStart, dateFinish, timeStart, timeFinish, idTeacher, days, active, studentsCapacity, price);
            return Json(validateCurseItem, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult createCurseItem(int idCurse, DateTime? dateStart, DateTime? dateFinish, DateTime? timeStart,
                                            DateTime? timeFinish, int idTeacher, int[] days ,string state, int active, int? studentsCapacity, int? price)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                curseItemService.createCurseItemService(idCurse, dateStart, dateFinish, timeStart, timeFinish, idTeacher, days, state, active, studentsCapacity, price, userSession.Id);
                return RedirectToAction("index", "CurseItem");
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpGet]
        public ActionResult formUpdateCurseItem(int id)
        {
            User userSession = Session["user"] as User;
            if (null != Session["user"] as User)
            {

                CurseItem curseItem = curseItemService.findCurseItemService(id);
    
                curseItemModel.daysItems = curseItemService.convertStringaIntArray(curseItem.ItemDays);
                curseItemModel.ListCurse = curseService.listCurseService();
                curseItemModel.listTeacher = teacherService.listTeacher();
                curseItemModel.listCurseItemState = curseItemStateService.listCurseItemStateService();
                curseItemModel.curseItem = curseItem;
                curseItemModel.listStudent = studentService.listStudentService();
                curseItemModel.userSession = userSession;
                return manageSession(userSession, curseItemModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public JsonResult updateValidateCurseItem(int idCurseItem, int idCurse, DateTime? dateStart, DateTime? dateFinish, DateTime? timeStart, DateTime? timeFinish, int idTeacher, int[] days, int active, int? studentsCapacity, decimal? price, int[] idStudents)
        {
            ValidateCurseItem validateCurseItem = curseItemService.validateCurseItemService(idCurse, dateStart, dateFinish, timeStart, timeFinish, idTeacher, days, active, studentsCapacity, price, idStudents, true, idCurseItem);
            return Json(validateCurseItem, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateCurseItem(int idCurseItem, int idCurse, DateTime? dateStart, DateTime? dateFinish, DateTime? timeStart, 
                                            DateTime? timeFinish, int idTeacher, int[] days, string state, int active, int? studentsCapacity, decimal? price, int[] idStudents)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                curseItemService.updateCurseItemService(idCurseItem, idCurse, dateStart, dateFinish, timeStart, timeFinish, idTeacher, days, state, active, studentsCapacity, price, userSession.Id, idStudents);
                return RedirectToAction("index", "CurseItem");
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpGet]
        public ActionResult detailCurseItem(int id)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                curseItemModel.curseItem = this.curseItemService.findCurseItemService(id);
                curseItemModel.userSession = userSession;
                return manageSession(userSession, curseItemModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        
        }

        [HttpPost]
        public ActionResult filterCurseItem(int active, int idTec)
        {
            if (null != Session["user"] as User)
            {
                curseItemModel.listCurseItems = curseItemService.filterCurseItemService(active, idTec);
                return View(curseItemModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public ActionResult teacherCurseItem(int idCurse)
        {
            curseItemModel.listTeacher = curseItemService.findTeacherCurseItemService(idCurse);
            return View(curseItemModel);
        }

        [HttpPost]
        public ActionResult findStudentCurseItem(string studentName)
        {
            ViewBag.studentName = studentName;
            curseItemModel.listStudent = curseItemService.findStudentCurseItemService(studentName);
            return View(curseItemModel);
        }
        
        public ActionResult manageSession(User userSession, CurseItemModel curseItemModel)
        {
            if (userSession.Rol.Id == 1)
            {
                return View(curseItemModel);
            }
            else
            {
                return RedirectToAction("index", "Teacher");
            }
        }
    }
}
