﻿using BackOffice.Models;
using BackOffice.Service;
using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOffice.Controllers
{
    public class LoginController : Controller
    {

        UserService userService = new UserService();
        LoginModel loginModel = new LoginModel();

        public ActionResult index()
        {
            loginModel.validateLogin =  TempData["validateLogin"] as ValidateLogin;
            loginModel.userSession = Session["user"] as User;
            return View(loginModel);
        }

        [HttpPost]
        public JsonResult validateLogin(string email, string password)
        {
            EncrypPassword encrypPassword = new EncrypPassword();
            string pass = encrypPassword.encrypt(password);
            ValidateLogin validateLogin = userService.validateLoginService(email, pass);
            return Json(validateLogin, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult signIn(string email, string password)
        {
            EncrypPassword encrypPassword = new EncrypPassword();
            string pass = encrypPassword.encrypt(password);
            User user = userService.signInService(email, pass);
            Session["user"] = user;
            return controlSession(user);
        }

        public ActionResult logOut()
        {
            Session.Clear();
            return RedirectToAction("index", "Login");
        }

        public ActionResult controlSession(User user)
        {
            if (user.Rol.Id == 1 )
            {
                return RedirectToAction("index", "User");
            }
            else
            {
                return RedirectToAction("index", "Teacher");
            }
        }


    }
}