﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOffice.Controllers
{
    public class SessionController : Controller
    {
        public User validateSession()
        {
            User user = new User();
            try
            {
                user = Session["user"] as User;
            }
            catch (Exception ex)
            {
                user = null;
            }
            return user;
        }
    }
}