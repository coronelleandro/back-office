﻿using BackOffice.Models;
using BackOffice.Service;
using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOffice.Controllers
{
    public class StudentController : Controller
    {
        private StudentService studentService = new StudentService();

        private StudentModel studentModel = new StudentModel();

        private SessionController sessionController = new SessionController();

        public ActionResult index()
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                studentModel.listStudent = studentService.listStudentService();
                studentModel.userSession = userSession;
                return manageSession(userSession, studentModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        public ActionResult formCreateStudent()
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                studentModel.userSession = userSession;
                return manageSession(userSession, studentModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public JsonResult validateStudent(string firstName,string lastName,string email,string dni,string telephone,string observations, HttpPostedFileBase file,int active)
        {
            ValidateStudent validateStudent = studentService.ValidateStudentService(firstName, lastName, email, dni, telephone, observations, file, active);
            return Json(validateStudent, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult createStudent(string firstName, string lastName, string email,string dni, string telephone, string observations, HttpPostedFileBase file,int active)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                studentService.createStudentService(firstName, lastName, email, dni, telephone, observations, file, active, userSession.Id);
                return RedirectToAction("index", "Student");
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public ActionResult mostrarImg(HttpPostedFileBase file)
        {

            var data = new byte[file.ContentLength];
            file.InputStream.Read(data, 0, file.ContentLength);
            string base64Data = Convert.ToBase64String(data);
            ViewBag.ImageData = base64Data;
            ViewBag.ImageContentType = file.ContentType;
            
            return View(); 
        } 

        [HttpGet]
        public ActionResult formUpdateStudent(int id)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                studentModel.student = studentService.findStudentService(id);
                studentModel.userSession = userSession;
                return manageSession(userSession, studentModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public JsonResult updateValidateStudent(string firstName, string lastName, string email, string dni, string telephone, string observations, HttpPostedFileBase file, int active)
        {
            ValidateStudent validateStudent = studentService.ValidateStudentService(firstName, lastName, email, dni, telephone, observations, file, active, true);
            return Json(validateStudent, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateStudent(int id, string firstName, string lastName, string email, string dni, string telephone, string observations, HttpPostedFileBase file, int active)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                studentService.updateStudentService(id, firstName, lastName, email, dni, telephone, observations, file, active);
                return RedirectToAction("index", "Student");
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpGet]
        public ActionResult detailStudent(int id)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                studentModel.student = studentService.findStudentService(id);
                studentModel.userSession = userSession;
                return manageSession(userSession, studentModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpGet]
        public ActionResult filterStudent(int active, string firstName, string lastName, string dni)
        {
            if (null != Session["user"] as User)
            {
                studentModel.listStudent = studentService.filterStudentService(active, firstName, lastName, dni);
                return View(studentModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        public ActionResult manageSession(User userSession, StudentModel studentModel)
        {
            if (userSession.Rol.Id == 1)
            {
                return View(studentModel);
            }
            else
            {
                return RedirectToAction("index", "Teacher");
            }
        }
    }
}