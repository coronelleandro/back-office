﻿using BackOffice.Models;
using BackOffice.Service;
using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace BackOffice.Controllers
{
    public class TeacherController : Controller
    {
        TeacherService teacherService = new TeacherService();
        TeacherModel teacherModel = new TeacherModel();
        TechnologyService technologyService = new TechnologyService();
        SessionController sessionController = new SessionController();
        CurseItemService curseItemService = new CurseItemService(); 


        public ActionResult index()
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                teacherModel.listTechnology = technologyService.listTechnologyService();
                teacherModel.listTeacher = teacherService.listTeacher();
                teacherModel.userSession = userSession;
                return View(teacherModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        public ActionResult formCreateTeacher()
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                teacherModel.listTechnology = technologyService.listTechnologyService();
                teacherModel.userSession = userSession;
                return manageSession(userSession, teacherModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public JsonResult validarTeacher(string firstName, string lastName, string email, string dni, string telephone, HttpPostedFileBase file, string linkedin, string obserbations, int active, int[] idTechnology)
        {
            ValidateTeacher validateTeacher = teacherService.validateTeacherService(firstName, lastName, email, dni, telephone, obserbations, linkedin, active, idTechnology);
            return Json(validateTeacher, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult createTeacher(string firstName, string lastName, string email, string dni, string telephone, HttpPostedFileBase file,string linkedin, string obserbations, int active,int[] idTechnology)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                this.teacherService.createTeacherRepository(firstName, lastName, email, dni, telephone, file, linkedin, obserbations, active, idTechnology, userSession.Id);
                return RedirectToAction("index", "Teacher");
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpGet]
        public ActionResult formUpdateTeacher(int id)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                teacherModel.teacher = teacherService.findTeacher(id);
                teacherModel.listTechnology = technologyService.listTechnologyService();
                teacherModel.userSession = userSession;
                return manageSession(userSession, teacherModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public JsonResult updateValidarTeacher(string firstName, string lastName, string email, string dni, string telephone, string file, string linkedin, string obserbations, int active, int[] idTechnology)
        {
            ValidateTeacher validateTeacher = teacherService.validateTeacherService(firstName, lastName, email, dni, telephone, obserbations, linkedin, active, idTechnology, true);
            return Json(validateTeacher, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateTeacher(int id,string firstName, string lastName, string email, string dni, string telephone, HttpPostedFileBase file, string linkedin, string obserbations, int active ,int[] idTechnology)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                this.teacherService.updateTeacherService(id, firstName, lastName, email, dni, telephone, file, linkedin, obserbations, active, idTechnology, userSession.Id);
                return RedirectToAction("index", "Teacher");   
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
           
        }

        [HttpPost]
        public ActionResult teacher(int[] idTechnoloy)
        {
            ViewBag.idTechnoloy = idTechnoloy;
            return View();
        }

       [HttpGet]
        public ActionResult detailTeacher(int id)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                teacherModel.teacher = teacherService.findTeacher(id);
                teacherModel.userSession = userSession;
                return manageSession(userSession, teacherModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        
        }

        [HttpPost]
        public ActionResult filterTeacher(int active,int idTec)
        {
            if (null != Session["user"] as User)
            {
                teacherModel.listTeacher = teacherService.filterTeacherService(active, idTec);
                return View(teacherModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        // teacher Mentor
        public ActionResult myCourses()
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                teacherModel.userSession = userSession;
                teacherModel.teacher = teacherService.findTeacherPersonService(userSession);
                return teacherSession(userSession, teacherModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public ActionResult myCoursesCalendar(int curseItem)
        {
            teacherModel.listTeacherCalendars = teacherService.listTeacherCalendarsService(curseItem);
            return View(teacherModel);
        }

        [HttpGet]
        public ActionResult teacherStudenCalendar(int id, int curseItemId, int countCal)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                teacherModel.userSession = userSession;
                teacherModel.listStudentCalendars = teacherService.listStudentCalendarsService(curseItemId, countCal);
                teacherModel.teacherCalendar = teacherService.teacherCalendarsClassService(id);
                teacherModel.curseItem = curseItemService.findCurseItemService(curseItemId);
                teacherModel.curseItemId = curseItemId;
                teacherModel.countCal = countCal;
                return teacherSession(userSession, teacherModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public ActionResult confirmAssistance(int[] assistance, int curseItem, int countCal)
        {
            User userSession = Session["user"] as User;
            teacherService.confirmAssistanceService(assistance, userSession, curseItem, countCal);
            return RedirectToAction("myCourses", "Teacher");
        }

        public ActionResult manageSession(User userSession, TeacherModel teacherModel)
        {
            if (userSession.Rol.Id == 1)
            {
                return View(teacherModel);
            }
            else
            {
                return RedirectToAction("index", "Teacher");
            }
        }

        //session mentor
        public ActionResult teacherSession(User userSession, TeacherModel teacherModel)
        {
            if (userSession.Rol.Id == 2)
            {
                return View(teacherModel);
            }
            else
            {
                return RedirectToAction("index", "User");
            }
        }
    }
}