﻿using BackOffice.Models;
using BackOffice.Service;
using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOffice.Controllers
{
    public class TechnologyController : Controller
    {
        TechnologyService technologyService = new TechnologyService();
        TechnologyModel technologyModel = new TechnologyModel();
        SessionController sessionController = new SessionController();

        public ActionResult index()
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                technologyModel.listTechnology = technologyService.listTechnologyService();
                technologyModel.userSession = userSession;
                return manageSession(userSession, technologyModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        public ActionResult formCreateTechnology()
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                technologyModel.listTechnology = technologyService.listTechnologyService();
                technologyModel.userSession = userSession;
                return manageSession(userSession, technologyModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public JsonResult validarTechnology(string name, int active)
        {
            ValidateTechnology validateTechnology = technologyService.validateTechnologyService(name, active);
            return Json(validateTechnology, JsonRequestBehavior.AllowGet);
        }

        public ActionResult createTechnology(string name, int active)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                technologyService.createRepositoryService(name, active, userSession.Id);
                return RedirectToAction("index", "Technology");
            }
            else
            {
                return RedirectToAction("index", "Login");
            } 
        }

        [HttpGet]
        public ActionResult formUpdateTechnology(int id)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                technologyModel.technology = technologyService.findTechnologyService(id);
                technologyModel.userSession = userSession;
                return manageSession(userSession, technologyModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public JsonResult updateValidarTechnology(int id, string name, int active)
        {
            ValidateTechnology validateTechnology = technologyService.validateTechnologyService(name, active, id, true);
            return Json(validateTechnology, JsonRequestBehavior.AllowGet);
        }

        public ActionResult updateTechnology(int id, string name, int active)
        {
            if (null != Session["user"] as User)
            {
                technologyService.updateTechnologyService(id, name, active);
                return RedirectToAction("index", "Technology");
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpGet]
        public ActionResult detailTechnology(int id)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                technologyModel.userSession = userSession;
                technologyModel.technology = technologyService.findTechnologyService(id);
                return manageSession(userSession, technologyModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }  
        }

        public ActionResult filterTechnology(int active)
        {
            if (null != Session["user"] as User)
            {
                technologyModel.listTechnology = technologyService.filterTechnologyActiveService(active);
                return View(technologyModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        public ActionResult manageSession(User userSession, TechnologyModel technologyModel)
        {
            if (userSession.Rol.Id == 1)
            {
                return View(technologyModel);
            }
            else
            {
                return RedirectToAction("index", "Teacher");
            }
        }
    }
}