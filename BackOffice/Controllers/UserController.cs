﻿using BackOffice.Models;
using BackOffice.Service;
using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackOffice.Controllers
{
    public class UserController : Controller
    {
        UserService userService = new UserService();
        RolService rolService = new RolService();
        UserModel userModel = new UserModel();
        TeacherService teacherService = new TeacherService();

        public ActionResult index()
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                userModel.listUser = userService.listUser();
                userModel.userSession = userSession;
                return manageSession(userSession, userModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
           
        }

        public ActionResult formCreateUser()
        {          
            userModel.userSession = Session["user"] as User;
            userModel.listRol = rolService.rolsService();
            userModel.listTeacher = teacherService.listTeacher();
            return View(userModel);
            
        }

        [HttpPost]
        public JsonResult validateUser(int rol, string firstName, string lastName, string email, int active, string password, string repeatPassword,int? teacherPerson)
        {
            ValidateUser validateUser = userService.ValidateUserService(firstName, lastName, email, active, password, repeatPassword, rol, teacherPerson);
            return Json(validateUser, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult createUser(int rol ,string firstName, string lastName, string email, string password, string repeatPassword, int active, int? teacherPerson)
        {            
            EncrypPassword encrypPassword = new EncrypPassword();
            string pass = encrypPassword.encrypt(password);
            this.userService.createUserService(rol,firstName, lastName, email, pass, repeatPassword, active, teacherPerson);
            return RedirectToAction("index", "User");            
        }

        [HttpGet]
        public ActionResult formUpdateUser(int id)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                userModel.userDetail = userService.findUserService(id);
                userModel.listRol = rolService.rolsService();
                userModel.listTeacher = teacherService.listTeacher();
                userModel.userSession = userSession;
                return manageSession(userSession, userModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpPost]
        public JsonResult updateValidateUser(int id, int rol, string firstName, string lastName, string email, string password, string repeatPassword, int active, int? teacherPerson)
        {
            ValidateUser validateUser = userService.ValidateUserService(firstName, lastName, email, active, password, repeatPassword, rol, teacherPerson, true, id);
            return Json(validateUser, JsonRequestBehavior.AllowGet);
        } 

        [HttpPost]
        public ActionResult updateUser(int rol, int id, string firstName, string lastName, string email, string password, string repeatPassword, int active, int? teacherPerson)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                EncrypPassword encrypPassword = new EncrypPassword();
                string pass = encrypPassword.encrypt(password);
                userService.uspdateUserService(rol, id, firstName, lastName, email, pass, repeatPassword, active, teacherPerson);
                return RedirectToAction("index", "User");
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpGet]
        public ActionResult detailUser(int id)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                userModel.userSession = userSession;
                userModel.userDetail = userService.findUserService(id);
                return manageSession(userSession, userModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        [HttpGet]
        public ActionResult filterUser(int active)
        {
            User userSession = Session["user"] as User;
            if (null != userSession)
            {
                userModel.listUser = userService.filterUserActiveService(active);
                return manageSession(userSession, userModel);
            }
            else
            {
                return RedirectToAction("index", "Login");
            }
        }

        public ActionResult manageSession(User userSession,UserModel userModel)
        {
            if (userSession.Rol.Id == 1)
            {
                return View(userModel);
            }
            else
            {
                return RedirectToAction("index", "Teacher");
            }
        }

    }   
}