﻿using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Models
{
    public class CurseItemModel
    {
        public List<CurseItem> listCurseItems { set; get; }

        public List<Curse> ListCurse { set; get; }

        public List<Teacher> listTeacher { set; get; }

        public CurseItem curseItem { set; get; }

        public List<CurseItemState> listCurseItemState { get; set; }

        public ValidateCurseItem validateCurseItem { get; set; }

        public List<Technology> listTechnologies { set; get; }

        public List<Student> listStudent { set; get; }

        public List<Person> listPerson { set; get; }

        public Student student { set; get; }

        public User userSession { set; get; }

        public List<int> daysItems { set; get; }
    }
}