﻿using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Models
{
    public class CurseModel
    {
        public List<Curse> listCurse { set; get; }

        public List<Technology> listTechnology { set; get; }

        public ValidateCurse validateCurse { set; get; }

        public Curse curse { set; get; }

        public User userSession { set; get; }
    }
}