﻿using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Models
{
    public class LoginModel
    {
        public Curse curse { set; get; }

        public ValidateLogin validateLogin{ set; get; }

        public User userSession { set; get; }
    }
}