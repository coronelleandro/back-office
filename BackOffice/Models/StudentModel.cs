﻿using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Models
{
    public class StudentModel
    {
        public Student student { set; get; }

        public List<Student> listStudent { set; get; }

        public User userSession { set; get; }

        public ValidateStudent validateStudent { set; get; }
    }
}