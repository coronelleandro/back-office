﻿using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Models
{
    public class TeacherModel
    {
        public List<Teacher> listTeacher { set; get; }

        public List<Technology> listTechnology { set; get; }

        public Teacher teacher { set; get; }

        public ValidateTeacher validateTeacher { set; get; } 

        public User userSession { set; get; }

        public List<TeacherCalendar> listTeacherCalendars { set; get; }

        public List<StudentCalendar> listStudentCalendars { set; get; }

        public int curseItemId { set; get; }
        
        public int countCal { set; get; }
        
        public CurseItem curseItem { set; get; }

        public TeacherCalendar teacherCalendar { set; get; }
    }
}