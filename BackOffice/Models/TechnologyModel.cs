﻿using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Models
{
    public class TechnologyModel
    {
        public List<Technology> listTechnology { set; get; }

        public Technology technology { set; get; }
        
        public ValidateTechnology validateTechnology { set; get; }

        public User userSession { set; get; }
    }
}