﻿using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Models
{
    public class UserModel
    {
        public List<User> listUser { set; get; }

        public ValidateUser validateUser { set; get; }

        public User userSession { get; set; }

        public User userDetail { get; set; }

        public List<Rol> listRol { set; get; }

        public List<Teacher> listTeacher { set; get; }
    }
}