﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Models
{
    public class ModelModel
    {
        public DateTime? dateStart { set; get; } 
        public DateTime? dateFinish { set; get; }
        public DateTime? timeStart { set; get; }
        public DateTime? timeEnd { set; get; }
        public DateTime? time { set; get; }
        public int[] days { set; get; }
    }
}