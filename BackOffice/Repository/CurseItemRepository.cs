﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BackOffice.Repository
{
    public class CurseItemRepository
    {
        private BackOfficeEntities context = new BackOfficeEntities();

        public List<CurseItem> listCurseItemsRepository()
        {
            return context.CurseItems.ToList();
        }

        public CurseItem findCurseItemsRepository(int id)
        {
            return context.CurseItems.Find(id);
        }

        public void createCurseItemRepository(int? idCurse, DateTime dateTimeStart, DateTime dateTimeEnd, int? idTeacher, string state, int active, int? studentsCapacity,string daysString,int[] days, decimal? price, int idUser)
        {
            CurseItem curseItem = new CurseItem();

            curseItem.DateStart = dateTimeStart;
            curseItem.DateFinish = dateTimeEnd;
            curseItem.DateState = DateTime.Now;
            curseItem.Teacher = context.Teachers.Find(idTeacher);
            curseItem.studentsCapacity = studentsCapacity.Value;
            curseItem.User = context.Users.Find(idUser);
            curseItem.DateCreate = 1;
            curseItem.ItemDays = daysString;
            curseItem.Price = price.Value;
            curseItem.Curse = context.Curses.Find(idCurse);
            curseItem.TeacherCalendars = createTeacherCalendar(idTeacher,idCurse,dateTimeStart,dateTimeEnd,idUser,days);

            CurseItemState curseItemState = new CurseItemState();
            curseItemState.active = Convert.ToBoolean(active);
            curseItemState.Name = state;
            curseItem.CurseItemState = curseItemState;
            
            context.CurseItems.Add(curseItem);
            context.SaveChanges();

        }

        public void deleteTeacherCalendar(int idCurseItem)
        {
            List<TeacherCalendar>listTeacherCalendar = context.TeacherCalendars.Where(tc => tc.CurseItem.Id == idCurseItem).ToList();
            foreach (TeacherCalendar ltc in listTeacherCalendar)
            {
                context.TeacherCalendars.Remove(ltc);
            }
            context.SaveChanges();
        }

        public void updateCurseItemRepository(int idCurseItem, int idCurse, DateTime dateTimeStart, DateTime dateTimeEnd, int idTeacher, string state, int active, int? studentsCapacity, string daysString, int[] days, decimal? price, int idUser, int[] idStudents)
        {
            CurseItem curseItem = context.CurseItems.Find(idCurseItem);

            curseItem.DateStart = dateTimeStart;
            curseItem.DateFinish = dateTimeEnd;
            curseItem.DateState = DateTime.Now;
            curseItem.Teacher = context.Teachers.Find(idTeacher);
            curseItem.studentsCapacity = studentsCapacity.Value;
            curseItem.User = context.Users.Find(idUser);
            curseItem.DateCreate = 1;
            curseItem.ItemDays = daysString;
            curseItem.Price = price.Value;
            curseItem.TeacherCalendars = createTeacherCalendar(idTeacher, idCurse, dateTimeStart, dateTimeEnd, idUser, days);
            curseItem.Curse = context.Curses.Find(idCurse);
            curseItem.CurseItemState.active = Convert.ToBoolean(active);
            curseItem.CurseItemState.Name = state;
            context.CurseItems.Attach(curseItem);
            context.Entry(curseItem).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void deleteStudentCalendar(int idCurseItem)
        {
            List<StudentCalendar> listStuCal = context.StudentCalendars.Where(cis => cis.CurseItem.Id == idCurseItem).ToList();
            foreach (StudentCalendar stuCal in listStuCal)
            {
                context.StudentCalendars.Remove(stuCal);
            }
            context.SaveChanges();
        }

        public void deleteCurseItemStudent(int idCurseItem)
        {
            List<CurseItemStudent> listCurseItemStudent = context.CurseItemStudents.Where(cis => cis.CurseItem.Id == idCurseItem).ToList();
            foreach (CurseItemStudent lcis in listCurseItemStudent)
            {
                context.CurseItemStudents.Remove(lcis);
            }
            context.SaveChanges();
        }


        public void createStudentCalendarFor(int idCurseItem, int[] idStudents, DateTime dateTimeStart, DateTime dateTimeEnd, int idUser,int[] days)
        {
            if (idStudents != null) {
                foreach (int idStu in idStudents)
                {
                    this.createStudentCalendar(idCurseItem, idStu, dateTimeStart, dateTimeEnd, idUser, days);
                }
            }
        }

        public void createStudentCalendar(int idCurseItem, int idStu, DateTime dateTimeStart, DateTime dateTimeEnd, int idUser,int[] days)
        {
            byte di = (byte)dateTimeStart.DayOfWeek;
            byte df = (byte)dateTimeEnd.DayOfWeek;
            TimeSpan diff = dateTimeEnd.Subtract(dateTimeStart);
            int daySum = di;
            int count = 0;
            DateTime dateTime = new DateTime(dateTimeStart.Year,dateTimeStart.Month,dateTimeStart.Day,dateTimeEnd.Hour,dateTimeEnd.Minute, dateTimeEnd.Second);
            for (int i = 0; i <= diff.Days; i++)
            {
                if (true == days.Contains(daySum))
                {
                    count += 1;
                    createNewStudentCalendar(idCurseItem,idStu,dateTimeStart.AddDays(i),dateTime.AddDays(i), idUser,days,count);
                }

                daySum += 1;
                if (daySum == 7)
                {
                    daySum = 0;
                }
            }
            //createNewStudentCalendar(idCurseItem,idStu,dateTimeStart.AddDays(i),dateTime.AddDays(i), idUser,days,count);
        }

        public void createNewStudentCalendar(int idCurseItem, int? idStu, DateTime dateTimeStart, DateTime dateTimeEnd, int idUser, int[] days, int count)
        {
            StudentCalendar studentCalendar = new StudentCalendar();
            studentCalendar.DateHourStart = dateTimeStart;
            studentCalendar.DateHourEnd = dateTimeEnd;
            studentCalendar.User = context.Users.Find(idUser);
            studentCalendar.Student = context.Students.Find(idStu);
            studentCalendar.CurseItem = context.CurseItems.Find(idCurseItem);
            studentCalendar.countCalendars = count;
            context.StudentCalendars.Add(studentCalendar);
            context.SaveChanges();
        }


        public void createCurserItenStudentFor(int idCurseItem, int[] idStudents, DateTime dateTimeStart, DateTime dateTimeEnd, int idUser)
        {
            if (idStudents != null)
            {
                foreach (int idStu in idStudents)
                {
                    createCurseItemStudent(idCurseItem, idStu, dateTimeStart, dateTimeEnd, idUser);
                }
            }
        }

        public void createCurseItemStudent(int idCurseItem, int idStu, DateTime dateTimeStart, DateTime dateTimeEnd, int idUser)
        {
            CurseItemStudent curseItemStudent = new CurseItemStudent();
            curseItemStudent.CurseItem = context.CurseItems.Find(idCurseItem);
            curseItemStudent.DateCreate = DateTime.Now;
            curseItemStudent.User = context.Users.Find(idUser);
            curseItemStudent.Student = context.Students.Find(idStu);
            context.CurseItemStudents.Add(curseItemStudent);
            context.SaveChanges();

        }

        public bool validateDateTimeTeacher(int? idTeacher, DateTime? dateStart, DateTime? dateFinish, DateTime? timeStart, DateTime? timeFinish)
        {
            DateTime dateTimeStart = new DateTime(dateStart.Value.Year, dateStart.Value.Month, dateStart.Value.Day, timeStart.Value.Hour, timeStart.Value.Minute, timeStart.Value.Second);

            DateTime dateTimeEnd = new DateTime(dateFinish.Value.Year, dateFinish.Value.Month, dateFinish.Value.Day, timeFinish.Value.Hour, timeFinish.Value.Minute, timeFinish.Value.Second);

            bool validate = false;
            List<TeacherCalendar> listTeacherCalendar = context.TeacherCalendars.Where(tt => tt.Teacher.Id == idTeacher).ToList();

            foreach (TeacherCalendar teacherCalendar in listTeacherCalendar) {
                if (teacherCalendar.DateHourStart.ToString("yyyy-MM-dd hh:mm") == dateTimeStart.ToString("yyyy-MM-dd hh:mm"))
                {
                    validate = true;
                }

            }
            return validate;
        }

        public bool validateUpdateDateTimeTeacher(int? idTeacher, DateTime? dateStart, DateTime? dateFinish, DateTime? timeStart, DateTime? timeFinish, int idCurseItem)
        {
            DateTime dateTimeStart = new DateTime(dateStart.Value.Year, dateStart.Value.Month, dateStart.Value.Day, timeStart.Value.Hour, timeStart.Value.Minute, timeStart.Value.Second);

            DateTime dateTimeEnd = new DateTime(dateFinish.Value.Year, dateFinish.Value.Month, dateFinish.Value.Day, timeFinish.Value.Hour, timeFinish.Value.Minute, timeFinish.Value.Second);


            bool validate = false;
            List<TeacherCalendar> listTeacherCalendar = context.TeacherCalendars.Where(tt => tt.Teacher.Id == idTeacher).Where(tt => tt.CurseItem.Id != idCurseItem).ToList();

            foreach (TeacherCalendar teacherCalendar in listTeacherCalendar)
            {
                if (teacherCalendar.DateHourStart.ToString("yyyy-MM-dd hh:mm") == dateTimeStart.ToString("yyyy-MM-dd hh:mm") || teacherCalendar.DateHourEnd.ToString("yyyy-MM-dd hh:mm") == dateTimeEnd.ToString(("yyyy-MM-dd hh:mm")))
                {
                    validate = true;
                }

            }
            return validate;
        }

        public List<TeacherCalendar> createTeacherCalendar(int? idTeacher,int? idCurse, DateTime dateTimeStart, DateTime dateTimeEnd, int idUser, int[] days)
        {
            List<TeacherCalendar> listTeacherCalendar = new List<TeacherCalendar>();
            byte di = (byte)dateTimeStart.DayOfWeek;
            byte df = (byte)dateTimeEnd.DayOfWeek;
            TimeSpan diff = dateTimeEnd.Subtract(dateTimeStart);
            int daySum = di;
            int count = 0;
            DateTime dateTime = new DateTime(dateTimeStart.Year, dateTimeStart.Month, dateTimeStart.Day, dateTimeEnd.Hour, dateTimeEnd.Minute, dateTimeEnd.Second);
            for (int i = 0; i <= diff.Days; i++)
            {
                if (true == days.Contains(daySum))
                {
                    count += 1;
                    listTeacherCalendar.Add(createNewTeacherCalendar(idTeacher,idCurse,dateTimeStart.AddDays(i),dateTime.AddDays(i),idUser,days,count));
                }

                daySum += 1;
                if (daySum == 7)
                {
                    daySum = 0;
                }

            }

            return listTeacherCalendar;
        }

        public TeacherCalendar createNewTeacherCalendar(int? idTeacher,int? idCurse, DateTime dateTimeStart, DateTime dateTimeEnd, int idUser, int[] days, int count)
        {
            TeacherCalendar teacherCalendar = new TeacherCalendar();
            teacherCalendar.DateHourStart = dateTimeStart; 
            teacherCalendar.DateHourEnd = dateTimeEnd;
            teacherCalendar.User = context.Users.Find(idUser);
            teacherCalendar.Teacher = context.Teachers.Find(idTeacher);
            teacherCalendar.countCalendars = count;
            return teacherCalendar; 
            /*context.TeacherCalendars.Add(teacherCalendar);
            context.SaveChanges();*/ 
        }
        

        public List<CurseItem> filterCurseItemRepository(int active, int idTechnology)
        {
            if (active == 0)
            {
                if (idTechnology == 0)
                {
                    return context.CurseItems.ToList();
                }
                else
                {
                    return context.CurseItems.Where(u => u.Curse.TechnologyId == idTechnology).ToList();
                }
            }
            else if (active == 1)
            {
                if (idTechnology == 0)
                {
                    return context.CurseItems.Where(c => c.CurseItemState.active == true).ToList();
                }
                else
                {
                    return context.CurseItems.Where(c => c.CurseItemState.active == true)
                               .Where(u => u.Curse.TechnologyId == idTechnology)
                               .ToList();
                }
            }
            else
            {
                if (idTechnology == 0)
                {
                    return context.CurseItems.Where(c => c.CurseItemState.active == false).ToList();
                }
                else
                {
                    return context.CurseItems.Where(c => c.CurseItemState.active == false)
                               .Where(u => u.Curse.TechnologyId == idTechnology)
                               .ToList();
                }
            }
        }

        public List<CurseItemState> curseItemsStateRepository()
        {
            return context.CurseItemStates.ToList();
        }

        public List<Teacher> findTeacherCurseItem(int idCurse)
        {
            List<Teacher> listTeacher = new List<Teacher>();

            var list = context.Teachers
                            .Join(
                                context.TeacherTechnologies, t => t.Id, tt => tt.Teacher.Id,
                                (t, tt) => new { t, tt })
                            .Join(
                                context.Technologies, ttx => ttx.tt.Technology.Id, tec => tec.Id,
                                (ttx, tec) => new { ttx, tec })
                            .Join(
                                context.Curses, ttxx => ttxx.tec.Id, c => c.Technology.Id,
                                (ttxx, c) => new { ttxx, c })
                            .Where(ttxxx => ttxxx.c.Id == idCurse)
                            .GroupBy(g => g.ttxx.ttx.t.Id)
                            .ToList();

            foreach (var l in list)
            {
                listTeacher.Add(context.Teachers.Find(l.Key));   
            }

            return listTeacher;
        }

        public List<Student> findStudentCurseItemRepository(string name)
        {
            return context.Students.Where(s => s.Person.FirstName.Contains(name)).Take(10).ToList();
        }

        public List<Student> addStudentCurseItemRepository(int idStudent, int[] idStudents)
        {
            List<Student> listStudent = context.Students.Where(s => s.Id == idStudent).ToList();
            
            foreach (int id in idStudents)
            {
                if (id != 0) {
                    listStudent.Add(context.Students.Find(id));
                }
            }
            
            return listStudent;
            
        }

        public bool validateStudentsCalendarFor(int[] idStudents,int? idCurseItem, DateTime? dateStart, DateTime? dateFinish, DateTime? timeStart, DateTime? timeFinish)
        {
            DateTime dateTimeStart = new DateTime(dateStart.Value.Year, dateStart.Value.Month, dateStart.Value.Day, timeStart.Value.Hour, timeStart.Value.Minute, timeStart.Value.Second);

            DateTime dateTimeEnd = new DateTime(dateFinish.Value.Year, dateFinish.Value.Month, dateFinish.Value.Day, timeFinish.Value.Hour, timeFinish.Value.Minute, timeFinish.Value.Second);

            int count = 0;
            if (idStudents!=null) {
                foreach (int idStu in idStudents)
                {
                    foreach (StudentCalendar sc in context.StudentCalendars.Where(sc => sc.Student.Id == idStu).Where(sc => sc.CurseItem.Id != idCurseItem).ToList())
                    {
                        if (sc.DateHourStart.ToString("yyyy-MM-dd hh:mm") == dateTimeStart.ToString("yyyy-MM-dd hh:mm") || sc.DateHourEnd.ToString("yyyy-MM-dd hh:mm") == dateTimeEnd.ToString("yyyy-MM-dd hh:mm"))
                        {
                            count += 1;
                        }
                    }
                }
            }
            if (count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

    }
}