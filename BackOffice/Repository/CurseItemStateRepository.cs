﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Repository
{
    public class CurseItemStateRepository
    {
        BackOfficeEntities context = new BackOfficeEntities();

        public List<CurseItemState> listCurseItemStates()
        {
            return context.CurseItemStates.ToList();
        }
    }
}