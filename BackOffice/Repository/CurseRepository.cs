﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BackOffice.Repository
{
    public class CurseRepository
    {
        private BackOfficeEntities context = new BackOfficeEntities();

        public List<Curse> listCurseRepository()
        {
            return context.Curses.ToList();
        }

        public void createCurseRepository(string name, string description, string requirements, string content, string img, int idTec, int active,int idUser)
        {
            Curse curse = new Curse();
            User user = context.Users.Find(idUser);
            curse.Descripcion = description.Trim();
            curse.Name = name.Trim();
            curse.Active = Convert.ToBoolean(active);
            curse.Requeriments = requirements.Trim();
            curse.ContentsTeached = content.Trim();
            curse.LinkPhoto = img;
            curse.User = user;
            curse.Technology = context.Technologies.Find(idTec); 
            context.Curses.Add(curse);
            context.SaveChanges();
        }

        public Curse findCurseIdRepository(int id)
        {
            return context.Curses.Find(id);
        }

        public void updateCurseRepository(int id, string description, string name, string requeriments, string contentsTeached, int idTec, string img, int active)
        {
            Curse curse = findCurseIdRepository(id);
            curse.Descripcion = description.Trim();
            curse.Name = name.Trim();
            curse.Requeriments = requeriments.Trim();
            curse.Active = Convert.ToBoolean(active);
            curse.ContentsTeached = contentsTeached.Trim();
            if (img!="") {
                curse.LinkPhoto = img;
            }
            //context.Curses.Add(curse);
            curse.Technology = context.Technologies.Find(idTec);
            context.Curses.Attach(curse);
            context.Entry(curse).State = EntityState.Modified;
            context.SaveChanges();
        }

        public List<Curse> filterCurseRepository(int active,int idTec)
        {
            if (active == 0)
            {
                if (idTec == 0) {
                    return context.Curses.ToList();
                }
                else
                {
                    return context.Curses.Where(c => c.Technology.Id == idTec).ToList();
                }
            }
            else if (active == 1)
            {
                if (idTec == 0)
                {
                    return context.Curses.Where(c=>c.Active==true).ToList();
                }
                else
                {
                    return context.Curses.Where(c => c.Active == true)
                           .Where(c => c.Technology.Id == idTec)
                           .ToList();
                }
            }
            else
            {
                if (idTec == 0)
                {
                    return context.Curses.Where(c => c.Active == false).ToList();
                }
                else
                {
                    return context.Curses.Where(c => c.Active == false)
                           .Where(c => c.Technology.Id == idTec)
                           .ToList();
                }
            }
          
        }

        public Curse findCurseNameRepository(string name)
        {
            return context.Curses.Where(c => c.Name == name).FirstOrDefault();
        }

        public Curse findCurseTechnologyRepository(int idTec)
        {
            return context.Curses.Where(c => c.Technology.Id == idTec).FirstOrDefault();
        }

    }
}
