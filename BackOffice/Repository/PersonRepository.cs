﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Repository
{
    public class PersonRepository
    {
        BackOfficeEntities context = new BackOfficeEntities();

        public Person createPersonRepository(string firstName, string lastName, string email, string dni, string telephone, string observations, string img)
        {
            Person person = new Person();
            person.Dni = dni.Trim();
            person.FirstName = firstName.Trim();
            person.LastName = lastName.Trim();
            person.Email = email.Trim();
            person.Telephone = telephone.Trim();
            person.LinkPhoto = img;
            person.Observations = observations.Trim();
            /*context.People.Add(person);
            context.SaveChanges();*/
            return person;
        }

        
    }
}