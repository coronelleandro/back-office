﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BackOffice.Repository
{
    public class StudentRepository
    {
        BackOfficeEntities context = new BackOfficeEntities();
        PersonRepository personRepository = new PersonRepository();

        public List<Student> listStudentRepository()
        {
            return context.Students.ToList(); 
        }

        public Student findStudentRepository(int id)
        {
            return context.Students.Find(id); 
        }

        public void createStudentRepository(string firstName, string lastName, string email, string dni, string telephone, string observations, string img, int active, int idUser)
        {
            Student student = new Student();
            student.Active = Convert.ToBoolean(active);
            student.User = this.context.Users.Find(idUser);
            Teacher teacher = context.Teachers.Where(t => t.Person.Dni == dni ).FirstOrDefault();
            if (teacher == null)
            {
                student.Person = personRepository.createPersonRepository(firstName, lastName, email, dni, telephone, observations, img);
            }
            else
            {
                student.Person = teacher.Person;
            }
            context.Students.Add(student);
            context.SaveChanges();
        }

        public void updateStudentRepository(int id,string firstName, string lastName, string email, string dni, string telephone, string observations, string img, int active)
        {

            Student student = this.findStudentRepository(id);
            student.Person.FirstName = firstName;
            student.Person.LastName = lastName;
            student.Person.Email = email;
            student.Person.Dni = dni;
            student.Person.Telephone = telephone;
            if (img!="") {
                student.Person.LinkPhoto = img;
            }
            student.Person.Observations = observations;
            student.Active = Convert.ToBoolean(active);
            context.Students.Attach(student);
            context.Entry(student).State = EntityState.Modified;
            context.SaveChanges();
        }

        public List<Student> filterStudentRepository(int active,string firstName, string lastName,string dni)
        {
            
            if (active == 1 || active==2)
            {
                bool bActive = active == 1; 
                if (firstName == "" && lastName == "" && dni == "" ) 
                    return this.context.Students.Where(s=> s.Active== bActive).ToList();                
                else                
                    return this.context.Students
                        .Where(s => s.Active == bActive && ( s.Person.FirstName.ToLower().Contains(firstName.ToLower()) || s.Person.LastName.ToLower().Contains(lastName.ToLower()) || s.Person.Dni.ToLower().Contains(dni.ToLower()))).ToList();
            }
            else
            {
                if (firstName == "" && lastName == "" && dni == "")                
                    return this.context.Students.ToList();                
                else                
                    return this.context.Students
                    .Where(s => s.Person.FirstName.ToLower().Contains(firstName.ToLower()) || s.Person.LastName.ToLower().Contains(lastName.ToLower()) || s.Person.Dni.ToLower().Contains(dni.ToLower()))
                    .ToList();                
            }
        }

        public Student findStudentDniRepository(string dni)
        {
            return context.Students.Where(s => s.Person.Dni == dni).FirstOrDefault();
        }

    }
}