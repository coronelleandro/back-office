﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BackOffice.Repository
{
    public class TeacherRepository
    {
        private BackOfficeEntities context = new BackOfficeEntities();
        private PersonRepository personRepository = new PersonRepository();
        private UserRepository userRepository = new UserRepository();

        public List<Teacher> listTeacherCurseItem(int idTec)
        {

            List<Teacher> listTeacher = new List<Teacher>();

            var list = context.Teachers
                       .Join(
                            context.TeacherTechnologies, t => t.Id, tt => tt.Teacher.Id,
                            (t, tt) => new { t, tt })
                       .Join(
                            context.Technologies, ttx => ttx.tt.Technology.Id, tec => tec.Id,
                            (ttx, tec) => new { ttx, tec })
                       .Where(ttxx => ttxx.tec.Id == idTec)
                       .GroupBy(t => t.ttx.t.Id)
                       .ToList();

            foreach (var i in list)
            {
                listTeacher.Add(context.Teachers.Find(i.Key));
            }

            return listTeacher;
        }

        public List<Teacher> listTeacherRepository()
        {
            return context.Teachers.ToList();
        }

        public Teacher findTeacherRepository(int id)
        {
            return context.Teachers.Find(id);
        }

        public void createTeacherRepository(string firstName, string lastName, string email, string dni, string telephone, string img, string linkedin, string obserbations, int active, int[] idTechnoloy, int idUser)
        {
            Teacher teacher = new Teacher();
            teacher.Linkedin = linkedin.Trim();
            teacher.Active = Convert.ToBoolean(active);
            foreach (int idT in idTechnoloy)
            {
                teacher.TeacherTechnologies.Add(asignTecnologiesRepository(idT, idUser));
            }
            teacher.User = this.context.Users.Find(idUser);
            Student student = context.Students.Where(s => s.Person.Dni == dni).FirstOrDefault();
            if (student == null)
            {
                teacher.Person = personRepository.createPersonRepository(firstName, lastName, email, dni, telephone, obserbations, img);
            }
            else
            {
                teacher.Person = student.Person;
            }
            
            context.Teachers.Add(teacher);
            context.SaveChanges();
            //this.createTeacherTechnology(idTec, user, teacher);
        }

        public TeacherTechnology asignTecnologiesRepository(int idT, int idUser)
        {
            TeacherTechnology teacherTechnology = new TeacherTechnology();
            teacherTechnology.Technology = context.Technologies.Find(idT);
            teacherTechnology.User = this.context.Users.Find(idUser);
            
            return teacherTechnology;
        }

        public void deleteTeacherRepository(int id)
        {
            Teacher teacher = context.Teachers.Find(id);
            List<TeacherTechnology> listTeacherTechnology = teacher.TeacherTechnologies.ToList();
            foreach (TeacherTechnology teacherTechnology in listTeacherTechnology)
            {
                context.TeacherTechnologies.Remove(teacherTechnology);
            }
            context.SaveChanges();

        }

        public void updateTeacherRepository(int id, string firstName, string lastName, string email, string dni, string telephone, string img, string linkedin, string obserbations, int active, int[] idTec, int idUser)
        {
            Teacher teacher = context.Teachers.Find(id);
            teacher.Linkedin = linkedin;
            
            teacher.Active = Convert.ToBoolean(active);
            
            if (img != "")
            {
                teacher.Person.LinkPhoto = img;
            }

            List<TeacherTechnology> listTeacherTechnology = teacher.TeacherTechnologies.ToList();
            
            foreach (int idT in idTec)
            {
                listTeacherTechnology.Add(asignTecnologiesRepository(idT, idUser));
            }
            teacher.TeacherTechnologies = listTeacherTechnology;
            teacher.User = this.context.Users.Find(idUser);
            teacher.Person.FirstName = firstName;
            teacher.Person.LastName = lastName;
            teacher.Person.Email = email;
            teacher.Person.Dni = dni;
            teacher.Person.Telephone = telephone;
            teacher.Person.Observations = obserbations;
           
            context.Teachers.Attach(teacher);
            context.Entry(teacher).State = EntityState.Modified;
            context.SaveChanges();
        }

        public List<Teacher> filterTeacherRepository(int active, int idTec)
        {

            List<Technology> listTechnology = new List<Technology>();
            List<Teacher> listTeacher = new List<Teacher>();

            if (active == 0)
            {
                var list = context.Teachers
                       .Join(
                            context.TeacherTechnologies, t => t.Id, tt => tt.Teacher.Id,
                            (t, tt) => new { t, tt })
                       .Join(
                            context.Technologies, ttx => ttx.tt.Technology.Id, tec => tec.Id,
                            (ttx, tec) => new { ttx, tec })
                       .Where(ttxx => ttxx.tec.Id == idTec)
                       .GroupBy(t => t.ttx.t.Id)
                       .ToList();

                foreach (var i in list)
                {
                    listTeacher.Add(context.Teachers.Find(i.Key));
                }
            }
            else if (active == 1)
            {
                var list = context.Teachers
                            .Join(
                                context.TeacherTechnologies, t => t.Id, tt => tt.Teacher.Id,
                                (t, tt) => new { t, tt })
                            .Join(
                                context.Technologies, ttx => ttx.tt.Technology.Id, tec => tec.Id,
                                (ttx, tec) => new { ttx, tec })
                            .Where(ttxx => ttxx.tec.Id == idTec)
                            .Where(ttxx => ttxx.ttx.t.Active == true)
                            .GroupBy(ttxx => ttxx.ttx.t.Id)
                            .ToList();

                foreach (var i in list)
                {
                    listTeacher.Add(context.Teachers.Find(i.Key));
                }

            }
            else
            {
                var list = context.Teachers
                           .Join(
                               context.TeacherTechnologies, t => t.Id, tt => tt.Teacher.Id,
                               (t, tt) => new { t, tt })
                           .Join(
                               context.Technologies, ttx => ttx.tt.Technology.Id, tec => tec.Id,
                               (ttx, tec) => new { ttx, tec })
                           .Where(ttxx => ttxx.tec.Id == idTec)
                           .Where(ttxx => ttxx.ttx.t.Active == false)
                           .GroupBy(ttxx => ttxx.ttx.t.Id)
                           .ToList();

                foreach (var i in list)
                {
                    listTeacher.Add(context.Teachers.Find(i.Key));
                }
            }

            return listTeacher;

        }

        public List<Teacher> filterTeacherDosRepository(int active)
        {
            if (active==0) {
                return this.context.Teachers.ToList();
            }
            else if (active==1)
            {
                return this.context.Teachers.Where(t => t.Active == true).ToList();
            }
            else
            {
                return this.context.Teachers.Where(t => t.Active == false).ToList();
            }
             
        }

        public List<TeacherTechnology> filterTeacherTecnhologyRepository(int idT)
        {

            return context.TeacherTechnologies.Where(t => t.Technology == context.Technologies.Find(idT)).ToList();
        }

        public Teacher teacherDniRepository(string dni)
        {
            return context.Teachers.Where(t => t.Person.Dni == dni).FirstOrDefault();
        }

        public Teacher findTeacherPersonRepository(User user)
        {
            var teacher = context.Users
                           .Join(
                                context.People, u => u.Person.Id, p => p.Id,
                                (u, p) => new { u, p })
                           .Join(
                                context.Teachers, pe => pe.p.Id, t => t.Person.Id,
                                (pe, t) => new { pe, t })
                           .Where(xx => xx.pe.u.Id == user.Id)
                           .ToList().FirstOrDefault();

            if (teacher != null) {
                return context.Teachers.Find(teacher.t.Id);
            }
            else
            {
                return null;
            }
        }

        public List<TeacherCalendar> listTeacherCalendarsRepository(int curseItem)
        {
            return context.TeacherCalendars.Where(tc=>tc.CurseItem.Id == curseItem).ToList();
        }

        public List<StudentCalendar> listStudentCalendarsRepository(int curseItem,int countCal)
        {
            return context.StudentCalendars.Where(sc => sc.CurseItem.Id == curseItem)
                   .Where(sc => sc.countCalendars == countCal).ToList();
        }

        public void confirmAssistanceRepository(int[] assistance, User user,int curseItem, int countCal)
        {
            List<StudentCalendar> listStedentCalendar = context.StudentCalendars.Where(sc => sc.CurseItem.Id == curseItem)
            .Where(sc => sc.countCalendars == countCal).ToList();

            if (assistance != null) {
                List<StudentCalendar> listStedentCalendar1 = context.StudentCalendars.Where(sc => assistance.Contains(sc.Id)).ToList();

                foreach (StudentCalendar sc in listStedentCalendar)
                {
                    if (listStedentCalendar1.Contains(sc))
                    {
                        context.StudentClassAssistances.Add(studentAssist(sc, user));
                    }
                    else
                    {
                        context.StudentClassAssistances.Add(studentNotAssist(sc, user));
                    }
                }

                context.SaveChanges();
            }
        }

        public StudentClassAssistance studentAssist(StudentCalendar sc, User user)
        {
            StudentClassAssistance assist = new StudentClassAssistance(); 
            assist.DateCreate = DateTime.Now;
            assist.assistance = true;
            assist.StudentCalendar = context.StudentCalendars.Find(sc.Id);
            assist.User = context.Users.Find(user.Id);
            return assist;
        }

        public StudentClassAssistance studentNotAssist(StudentCalendar sc, User user)
        {
            StudentClassAssistance assist = new StudentClassAssistance();
            assist.DateCreate = DateTime.Now;
            assist.assistance = false;
            assist.StudentCalendar = context.StudentCalendars.Find(sc.Id);
            assist.User = context.Users.Find(user.Id);
            return assist;
        }

        public void deleteStudentAssistanceRepository(int curseItem, int countCal)
        {
            List<StudentClassAssistance> studentAssit = context.StudentClassAssistances
                                                        .Where(sc => sc.StudentCalendar.CurseItem.Id == curseItem)
                                                        .Where(sc => sc.StudentCalendar.countCalendars == countCal)
                                                        .ToList();

            foreach (StudentClassAssistance sca in studentAssit)
            {
                context.StudentClassAssistances.Remove(sca);
            }
            context.SaveChanges();
           
        } 

        public TeacherCalendar teacherCalendarsClassRepository(int id)
        {
            return context.TeacherCalendars.Find(id);
        }
    }
}