﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BackOffice.Repository
{
    public class TechnologyRepository
    {
        private BackOfficeEntities context = new BackOfficeEntities();

        public List<Technology> listTechnologyRepository()
        {
            return context.Technologies.ToList();
        }

        public Technology findTechnologyRepository(int id)
        {
            return this.context.Technologies.Find(id); 
        }

        public void createRepositoryRepository(string name,int active,int id)
        {
            Technology newTechnology = new Technology();
            newTechnology.User = context.Users.Find(id);
            newTechnology.Active = Convert.ToBoolean(active);
            newTechnology.Name = name;
            context.Technologies.Add(newTechnology);
            context.SaveChanges();
        }

        public void updateTechnologyRepository(int id, string name, int active)
        {
            Technology technology = this.findTechnologyRepository(id);
            technology.User = context.Users.Find(id);
            technology.Active = Convert.ToBoolean(active);
            technology.Name = name;
            context.Technologies.Attach(technology);
            context.Entry(technology).State = EntityState.Modified;
            context.SaveChanges();
        }


        public List<Technology> filterTechnologyActiveRepository(int active)
        {
            if (active == 0)
            {
                return context.Technologies.ToList();
            }
            else if (active == 1)
            {
                return context.Technologies.Where(u => u.Active == true).ToList();
            }
            else
            {
                return context.Technologies.Where(u => u.Active == false).ToList();
            }
        }

        public Technology findTechnologyNameRepository(string name,int idTec=0)
        {
            if (idTec==0) {
                return this.context.Technologies.Where(t => t.Name == name).FirstOrDefault();
            }
            else
            {
                return this.context.Technologies.Where(t => t.Name == name && t.Id != idTec).FirstOrDefault();
            }
        }


    }
}