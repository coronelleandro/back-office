﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BackOffice.Repository
{
    public class UserRepository
    {
        private BackOfficeEntities context = new BackOfficeEntities();

        public List<User> listUserRepository()
        {
            return context.Users.ToList();
        }

        public void createUserRepository(int rol,string firstName, string lastName, string email, string password, string repeatPassword, int active, int? teacherPerson)
        {
            User newUser = new User();
            newUser.FirstName = firstName.Trim();
            newUser.LastName = lastName.Trim();
            newUser.Email = email.Trim();
            newUser.Rol = context.Rols.Find(rol);
            newUser.Password = password.Trim();
            //newUser.p
            if (null != teacherPerson)
            {
                newUser.Person = context.People.Find(teacherPerson);
            }
            newUser.Active = Convert.ToBoolean(active);
            context.Users.Add(newUser);
            context.SaveChanges();
        }

        public User findUserIdRepository(int id)
        {
            return context.Users.Find(id);
        }

        public void updateUserRepository(int rol,int idUser, string firstName, string lastName, string email, string password, string repeatPassword, int active, int? teacherPerson)
        {
            User user = findUserIdRepository(idUser);
            user.FirstName = firstName.Trim();
            user.LastName = lastName.Trim();
            user.Email = email.Trim();
            if (password != "") {
                user.Password = password;
            }
            user.Rol = context.Rols.Find(rol);
            if (null != teacherPerson)
            {
                user.Person = context.People.Find(teacherPerson);
                user.personId = teacherPerson.Value;
            }
            else
            {
                user.Person = null;
                user.personId = null;
            }
            user.Active = Convert.ToBoolean(active);
            context.Users.Attach(user);
            context.Entry(user).State = EntityState.Modified;
            context.SaveChanges();
        }

        public User signInRepository(string email,string password)
        {
            return context.Users.Where(u => u.Email == email)
                   .Where(u => u.Password == password)
                   .FirstOrDefault();
        }

        public int finNameUserRepository(string email)
        {
            return context.Users.Where(u => u.Email == email).ToList().Count;
        }

        public List<User> filterUserActiveRepository(int active)
        {
            if (active == 0) {
                return context.Users.ToList();
            }
            else if (active == 1)
            {
                return context.Users.Where(u => u.Active == true).ToList();
            }
            else
            {
                return context.Users.Where(u => u.Active == false).ToList();
            }
        }

        public User finUserEmailRepository(string email)
        {
            return context.Users.Where(u => u.Email == email).FirstOrDefault();  
        } 

        public User linkedUserTeacher(int? idPerson,int? idUser)
        {
            if (null == idPerson)
            {
                return null;
            }
            else
            {
                if (idUser == null)
                {
                    return context.Users.Where(u => u.Person.Id == idPerson).FirstOrDefault();
                }
                else
                {
                    return context.Users.Where(u => u.Person.Id == idPerson)
                           .Where(u => u.Id != idUser.Value).FirstOrDefault();
                }
            }
        }  
    }
}