﻿using BackOffice.Repository;
using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Service
{
    public class CurseItemService
    {
        CurseItemRepository curseItemRepository = new CurseItemRepository();

        public List<CurseItem> listCurseItemsService()
        {
            return curseItemRepository.listCurseItemsRepository();
        }

        public CurseItem findCurseItemService(int id)
        {
            return this.curseItemRepository.findCurseItemsRepository(id);
        }

        public void createCurseItemService(int? idCurse, DateTime? dateStart, DateTime? dateFinish, DateTime? timeStart,
                                            DateTime? timeFinish, int? idTeacher, int[] days, string state, int active, int? studentsCapacity, 
                                            decimal? price ,int idUser)
        {
            

            string daysString = "";
            if (days!=null) {
                for (var i = 0; i < days.Length; i++)
                {
                    if (i == days.Length - 1)
                    {
                        daysString += days[i];
                    }
                    else
                    {
                        daysString += days[i] + "-";
                    }
                }
            }

            DateTime dateTimeStart = new DateTime(dateStart.Value.Year, dateStart.Value.Month, dateStart.Value.Day, timeStart.Value.Hour, timeStart.Value.Minute, timeStart.Value.Second);

            DateTime dateTimeEnd = new DateTime(dateFinish.Value.Year, dateFinish.Value.Month, dateFinish.Value.Day, timeFinish.Value.Hour, timeFinish.Value.Minute, timeFinish.Value.Second);

          
            //curseItemRepository.createTeacherCalendar(idTeacher, idCurse, dateTimeStart,dateTimeEnd,idUser, days);

            curseItemRepository.createCurseItemRepository(idCurse, dateTimeStart, dateTimeEnd, idTeacher, state, active, studentsCapacity, daysString,days, price, idUser);
        }


        //update
        public void updateCurseItemService(int idCurseItem,int idCurse,DateTime? dateStart, DateTime? dateFinish, DateTime? timeStart, DateTime? timeFinish,int idTeacher, int[] days, string state, int active, int? studentsCapacity,decimal? price, int idUser, int[] idStudents)
        {
            string daysString = "";
            if (days!=null) {
                for (var i = 0; i < days.Length; i++)
                {
                    if (i == days.Length - 1)
                    {
                        daysString += days[i];
                    }
                    else
                    {
                        daysString += days[i] + "-";
                    }
                }
            }


            DateTime dateTimeStart = new DateTime(dateStart.Value.Year, dateStart.Value.Month, dateStart.Value.Day, timeStart.Value.Hour,
                                                  timeStart.Value.Minute, timeStart.Value.Second);

            DateTime dateTimeEnd = new DateTime(dateFinish.Value.Year, dateFinish.Value.Month, dateFinish.Value.Day, timeFinish.Value.Hour,
                                                timeFinish.Value.Minute, timeFinish.Value.Second);

            curseItemRepository.deleteCurseItemStudent(idCurseItem);

            curseItemRepository.deleteTeacherCalendar(idCurseItem);

            curseItemRepository.deleteStudentCalendar(idCurseItem);

            curseItemRepository.createCurserItenStudentFor(idCurseItem, idStudents, dateTimeStart, dateTimeEnd, idUser);

            curseItemRepository.createStudentCalendarFor(idCurseItem, idStudents, dateTimeStart,dateTimeEnd,idUser,days);

            curseItemRepository.updateCurseItemRepository(idCurseItem, idCurse, dateTimeStart, dateTimeEnd, idTeacher, state, active, studentsCapacity, daysString, days, price, idUser, idStudents);
        }


        //validate
        public ValidateCurseItem validateCurseItemService(int idCurse, DateTime? dateStart, DateTime? dateFinish, DateTime? timeStart,DateTime? timeFinish, int? idTeacher, int[] days, int active ,int? studentsCapacity,decimal? price, int[] idStudents = null,bool update = false, int? idCurseItem = null)
        {
            ValidateCurseItem validateCurseItem = new ValidateCurseItem();
            
            validateCurseItem.countError = 0;

            //nombre
            if (dateStart == null)
            {
                validateCurseItem.dateStartError = "La fecha de incio es obligatoria";
                validateCurseItem.countError += 1;
            }

            if (dateFinish == null)
            {
                validateCurseItem.dateEndError = "La fecha de finalizacion es obligatoria";
                validateCurseItem.countError += 1;
            }

            if (timeStart == null)
            {
                validateCurseItem.timeStartError = "La hora de inicio es obligatoria";
                validateCurseItem.countError += 1;
            }

            if (timeFinish == null)
            {
                validateCurseItem.timeEndError = "La hora de finalizacion es obligatoria";
                validateCurseItem.countError += 1;
            }

            if (days == null)
            {
                validateCurseItem.daysError = "Tiene que elegir algun dia de la semana";
                validateCurseItem.countError += 1;
            }
            else if(days.Length == 0)
            {
                validateCurseItem.daysError = "Tiene que elegir algun dia de la semana";
                validateCurseItem.countError += 1;
            }

            if (studentsCapacity == null )
            {
                validateCurseItem.studentsCapacityError = "La capacidad maxima de alumnos es requerida";
                validateCurseItem.countError += 1;
            }

            if (price == null)
            {
                validateCurseItem.priceError = "El precio es obligatoria";
                validateCurseItem.countError += 1;
            }
            
            if (update == false) {
                if (idTeacher == 0)
                {
                    validateCurseItem.teacherError = "El profesor es requerido";
                    validateCurseItem.countError += 1;
                }
                else if (dateStart == null || dateFinish == null || timeStart == null || timeFinish == null)
                {
                    validateCurseItem.teacherError = "Las fechas no fueron bien definidas";
                    validateCurseItem.countError += 1;
                }
                else if (true == curseItemRepository.validateDateTimeTeacher(idTeacher, dateStart, dateFinish, timeStart, timeFinish))
                {
                    validateCurseItem.teacherError = "El profesor no esta disponible en ese horario";
                    validateCurseItem.countError += 1;
                }
            }
            else
            {
                if (idTeacher == 0 || idTeacher == null)
                {
                    validateCurseItem.teacherError = "El profesor es requerido";
                    validateCurseItem.countError += 1;
                }
                else if (dateStart == null || dateFinish == null || timeStart == null || timeFinish == null)
                {
                    validateCurseItem.teacherError = "Las fechas no fueron bien definidas";
                    validateCurseItem.countError += 1;
                }
                else if (true == curseItemRepository.validateUpdateDateTimeTeacher(idTeacher, dateStart, dateFinish, timeStart, timeFinish,idCurseItem.Value))
                {
                    validateCurseItem.teacherError = "El profesor no esta disponible en ese horario";
                    validateCurseItem.countError += 1;
                }
            }

            if (update!=false)
            {
                if (idStudents != null) {
                    if (studentsCapacity < idStudents.Length)
                    {
                        validateCurseItem.studentsCapacityError = "Se excedio el limite de alumnos";
                        validateCurseItem.countError += 1;
                    }

                }
                if (dateStart != null && dateFinish != null && timeStart != null && timeFinish != null)
                {
                    if (true == curseItemRepository.validateStudentsCalendarFor(idStudents, idCurseItem, dateStart, dateFinish, timeStart, timeFinish))
                    {
                        validateCurseItem.studentError = "Uno o mas alumnos no pueden ser asignados por problemas de calendario";
                        validateCurseItem.countError += 1;
                    }
                }
            }

            return validateCurseItem;
        }



        public List<CurseItem> filterCurseItemService(int active, int idTechnology)
        {
            return curseItemRepository.filterCurseItemRepository(active, idTechnology);
        }

        public List<CurseItemState> curseItemsStateService()
        {
            return curseItemRepository.curseItemsStateRepository();
        }


        public List<Teacher> findTeacherCurseItemService(int idCurse)
        {
            return curseItemRepository.findTeacherCurseItem(idCurse);
        }

        public List<Student> findStudentCurseItemService(string studentName)
        {
            return curseItemRepository.findStudentCurseItemRepository(studentName);
        }

        public List<Student> addStudentCurseItemService(int idStudent,int[] idStudents)
        {
            return curseItemRepository.addStudentCurseItemRepository(idStudent, idStudents);
        }

        public List<int> convertStringaIntArray(string days)
        {
            List<int> daysInt = new List<int>();  
            for(int i = 0; i < days.Length; i++)
            {
                if (days[i] >= '0' && days[i] <= '9')
                {
                    daysInt.Add(Convert.ToInt32(Convert.ToInt64(days[i].ToString())));
                }
                    
            }
            return daysInt;
        }
    }
}