﻿using BackOffice.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Service
{
    public class CurseItemStateService
    {
        CurseItemStateRepository curseItemStateRepository = new CurseItemStateRepository();

        public List<CurseItemState> listCurseItemStateService()
        {
            return this.curseItemStateRepository.listCurseItemStates();
        }
    }
}