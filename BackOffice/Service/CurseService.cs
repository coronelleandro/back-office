﻿using BackOffice.Repository;
using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Service
{
    public class CurseService
    {
        CurseRepository curseRepository = new CurseRepository();

        public List<Curse> listCurseService()
        {
            return curseRepository.listCurseRepository();
        }

        public void createCurseService(string name, string description, string requirements, string content, HttpPostedFileBase file, int idTec, int active,int idUser)
        {
            string img = "";
            if (file != null) {
                var data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);
                string base64Data = Convert.ToBase64String(data);
                img = base64Data;
            }
            curseRepository.createCurseRepository(name, description, requirements, content, img, idTec, active ,idUser);
        }

        public Curse findCurseService(int id)
        {
            return curseRepository.findCurseIdRepository(id);
        }

        public void updateCurseService(int id, string description, string name, string requeriments, string contentsTeached, int idTec, HttpPostedFileBase file,int active)
        {
            string img = "";
            if (file!=null) {
                var data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);
                string base64Data = Convert.ToBase64String(data);
                img = base64Data;
            }
            curseRepository.updateCurseRepository(id, description, name, requeriments, contentsTeached, idTec, img, active);
        }

        public ValidateCurse validateCurseService(string name, string description, string requirements, string content, int? idTec,string file, int active,bool update=false)
        {
            ValidateCurse validateCurse = new ValidateCurse();

            validateCurse.countError = 0;
            if (name == "" )
            {
                validateCurse.nameError = "El nombre del curso es requerido";
                validateCurse.countError += 1;
            }
            else if (name.Length > 250 )
            {
                validateCurse.nameError = "El nombre no pude excesder los 250 caracteres";
                validateCurse.countError += 1;
            }
            else if (null != curseRepository.findCurseNameRepository(name))
            {
                if (update == false)
                {
                    validateCurse.nameError = "El nombre del curso ya esta registrado";
                    validateCurse.countError += 1;
                }
            }

            //description
            if (description == "" || requirements == null)
            {
                validateCurse.descriptionError = "La decripcion del curso es requerido";
                validateCurse.countError += 1;
            }
            else if (description.Length > 2500)
            {
                validateCurse.descriptionError = "La descripcion no puede superar los 2500 caracteres";
                validateCurse.countError += 1;
            }

            //Requerimientos
            if (requirements == "" || requirements == null)
            {
                validateCurse.requerimentsError = "El requerimiento del curso es requerido";
                validateCurse.countError += 1;
            }
            else if (requirements.Length > 2500)
            {
                validateCurse.requerimentsError = "El requerimiento no puede los 2500 caracteristicas";
                validateCurse.countError += 1;
            }

            //contenido
            if (content == "" || content == null)
            {
                validateCurse.contentsTeachedError = "El contenido del curso es requerido";
                validateCurse.countError += 1;
            }
            else if (content.Length > 2500)
            {
                validateCurse.contentsTeachedError = "El contenido no puede los 2500 caracteristicas";
                validateCurse.countError += 1;
            }

            //technology
            if (idTec == null)
            {
                validateCurse.technologyError = "No eligio ninguna tecnologia";
                validateCurse.countError += 1;
            }
            else if (null != curseRepository.findCurseTechnologyRepository(idTec.Value))
            {
                if (update == false) {
                    validateCurse.technologyError = "Esta tecnologia ya fue asignada a otro curso";
                    validateCurse.countError += 1;
                }
            }

            //file
            if (file == "")
            {
                if (update == false) {
                    validateCurse.linkPhotoError = "La imagen es requerida";
                    validateCurse.countError += 1;
                }
            }

            return validateCurse;
        }

        public List<Curse> filterCurseService(int active, int idTec)
        {
            return this.curseRepository.filterCurseRepository(active, idTec);
        }
    }
}