﻿using BackOffice.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Service
{
    public class RolService
    {
        private RolRepository rolRepository = new RolRepository();

        public List<Rol> rolsService()
        {
            return rolRepository.rolsRepository();
        }
    }
}