﻿using BackOffice.Repository;
using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BackOffice.Service
{
    public class StudentService
    {
        StudentRepository studentRepository = new StudentRepository();

        public List<Student> listStudentService()
        {
            return studentRepository.listStudentRepository();
        }

        public void createStudentService(string firstName, string lastName, string email, string dni, string telephone, string observations, HttpPostedFileBase file, int active, int idUser)
        {
            string img = "";
            if (file != null)
            {
                var data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);
                string base64Data = Convert.ToBase64String(data);
                img = base64Data;
            }
            studentRepository.createStudentRepository(firstName, lastName, email, dni, telephone, observations, img, active, idUser);
        }

        public Student findStudentService(int id)
        {
            return this.studentRepository.findStudentRepository(id);
        }

        public void updateStudentService(int id, string firstName, string lastName, string email, string dni, string telephone, string observations, HttpPostedFileBase file, int active)
        {
            string img = "";
            if (file!=null) {
                var data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);
                string base64Data = Convert.ToBase64String(data);
                img = base64Data;
            }
            studentRepository.updateStudentRepository(id, firstName, lastName, email, dni, telephone, observations, img, active);
        }

        public ValidateStudent ValidateStudentService(string firstName, string lastName, string email, string dni, string telephone, string observations, HttpPostedFileBase file, int active,bool update = false )
        {
            ValidateStudent validateStudent = new ValidateStudent();
            validateStudent.countError = 0;

            //nombre
            if (firstName == "" || firstName == null)
            {
                validateStudent.firstNameError = "El nombre es requerido";
                validateStudent.countError += 1;
            }
            else if (firstName.Length > 200)
            {
                validateStudent.firstNameError = "El nombre no puede exeder los 200 caracteres";
                validateStudent.countError += 1;
            }

            //apellido
            if (lastName == "")
            {
                validateStudent.lastNameError = "El apellido es requerido";
                validateStudent.countError += 1;
            }
            else if (lastName.Length > 200)
            {
                validateStudent.lastNameError = "El apellido no puede exeder los 200 caracteres";
                validateStudent.countError += 1;
            }

            //email
            if (email == "")
            {
                validateStudent.emailError = "El email es requerido";
                validateStudent.countError += 1;
            }
            else if (email.Length > 150)
            {
                validateStudent.emailError = "El email no puede exeder los 150 caracteres";
                validateStudent.countError += 1;
            }
            else if (false == validateEmailService(email))
            {
                validateStudent.emailError = "El email no esta bien escrito";
                validateStudent.countError += 1;
            }

            //telephone
            if (telephone == "")
            {
                validateStudent.telephoneError = "El telefono es requerido";
                validateStudent.countError += 1;
            }
            else if (telephone.Length > 150)
            {
                validateStudent.emailError = "El telefono no puede exeder los 150 caracteres";
                validateStudent.countError += 1;
            }

            //dni   
            if (dni == "")
            {
                validateStudent.dniError = "El dni es requerido";
                validateStudent.countError += 1;
            }
            else if (dni.Length > 50)
            {
                validateStudent.dniError = "El dni no puede exeder los 50 caracteres";
                validateStudent.countError += 1;
            }
            else if(null != studentRepository.findStudentDniRepository(dni))
            {
                if (update==false) {
                    validateStudent.dniError = "El dni ya esta registrado a oto alumno";
                    validateStudent.countError += 1;
                }
            }

            //file
            /*if (file == null)
            {
                if (update == false)
                {
                    validateStudent.imgError = "La imagen es requerida";
                    validateStudent.countError += 1;
                }
            }*/

            return validateStudent;
        }
       

        public List<Student> filterStudentService(int active, string firstName, string lastName, string dni)
        {
            return this.studentRepository.filterStudentRepository(active, firstName, lastName, dni);
        }

        public bool validateEmailService(string email)
        {
            string expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        } 
    }
}