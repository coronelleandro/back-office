﻿using BackOffice.Repository;
using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BackOffice.Service
{
    public class TeacherService
    {
        private TeacherRepository teacherRepository = new TeacherRepository();

        public List<Teacher> listTeacherCurseItemService(int idTec)
        {
            return teacherRepository.listTeacherCurseItem(idTec); 
        }

        public List<Teacher> listTeacher()
        {
            return teacherRepository.listTeacherRepository();
        }

        public Teacher findTeacher(int id)
        {
            return teacherRepository.findTeacherRepository(id);
        }

        public void createTeacherRepository(string firstName, string lastName, string email, string dni, string telephone, HttpPostedFileBase file, string linkedin, string obserbations, int active,int[] idTechnoloy, int idUser)
        {
            string img = "";
            if (file != null)
            {
                var data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);
                string base64Data = Convert.ToBase64String(data);
                img = base64Data;
            }
            this.teacherRepository.createTeacherRepository(firstName, lastName,email, dni, telephone, img, linkedin, obserbations, active, idTechnoloy, idUser);
        }


        //update
        public void updateTeacherService(int id,string firstName, string lastName, string email, string dni, string telephone, HttpPostedFileBase file, string linkedin, string obserbations, int active, int[] idTec, int idUser)
        {
            string img = "";
            if (file!=null) {
                var data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);
                string base64Data = Convert.ToBase64String(data);
                img = base64Data;
            }
            this.teacherRepository.deleteTeacherRepository(id);
            this.teacherRepository.updateTeacherRepository(id, firstName, lastName, email, dni, telephone, img, linkedin, obserbations, active, idTec, idUser);
        }

        public ValidateTeacher validateTeacherService(string firstName, string lastName, string email, string dni, string telephone, string observations,string linkedin,int active, int[] idTechnology, bool? updateTeacher = null)
        {
            ValidateTeacher validateTeacher = new ValidateTeacher();
            validateTeacher.countError = 0;

            //nombre
            if (firstName == "" || firstName == null)
            {
                validateTeacher.firstNameError = "El nombre es requerido";
                validateTeacher.countError += 1;
            }
            else if (firstName.Length > 200)
            {
                validateTeacher.firstNameError = "El nombre no puede exeder los 200 caracteres";
                validateTeacher.countError += 1;
            }

            //apellido
            if (lastName == "" || lastName == null)
            {
                validateTeacher.lastNameError = "El apellido es requerido";
                validateTeacher.countError += 1;
            }
            else if (lastName.Length > 200)
            {
                validateTeacher.lastNameError = "El apellido no puede exeder los 200 caracteres";
                validateTeacher.countError += 1;
            }

            //email
            if (email == "" || email == null)
            {
                validateTeacher.emailError = "El email es requerido";
                validateTeacher.countError += 1;
            }
            else if (email.Length > 150)
            {
                validateTeacher.emailError = "El email no puede exeder los 150 caracteres";
                validateTeacher.countError += 1;
            }
            else if (false == validateEmailService(email))
            {
                validateTeacher.emailError = "El email esta mal escrito";
                validateTeacher.countError += 1;
            }

            //telephone
            if (telephone == "" || telephone==null)
            {
                validateTeacher.telephoneError = "El telefono es requerido";
                validateTeacher.countError += 1;
            }
            else if (telephone.Length > 150)
            {
                validateTeacher.emailError = "El telefono no puede exeder los 150 caracteres";
                validateTeacher.countError += 1;
            }

            //dni   
            if (dni == "" || dni == null)
            {
                validateTeacher.dniError = "El dni es requerido";
                validateTeacher.countError += 1;
            }
            else if (dni.Length > 50)
            {
                validateTeacher.dniError = "El dni no puede exeder los 50 caracteres";
                validateTeacher.countError += 1;
            }
            else if (null != teacherRepository.teacherDniRepository(dni))
            {
                if (updateTeacher == null)
                {
                    validateTeacher.dniError = "El dni ya esta registrado a oto profesor";
                    validateTeacher.countError += 1;
                }
            }

            if (idTechnology == null || idTechnology.Length == 0)
            {
                validateTeacher.technologyError = "A este profesor no se le asigno ninguna tecnologia";
                validateTeacher.countError += 1;
            }

            return validateTeacher;
        }


        public List<Teacher> filterTeacherService(int active, int idTec)
        {
            if (idTec != 0)
            {
                return this.teacherRepository.filterTeacherRepository(active, idTec);
            }
            else{
                return this.teacherRepository.filterTeacherDosRepository(active);
            }
           
        }

        public bool validateEmailService(string email)
        {
            string expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public Teacher findTeacherPersonService(User user)
        {
            return teacherRepository.findTeacherPersonRepository(user); 
        }

        public List<TeacherCalendar> listTeacherCalendarsService(int curseItem)
        {
            return teacherRepository.listTeacherCalendarsRepository(curseItem);
        }

        public List<StudentCalendar> listStudentCalendarsService(int curseItem,int countCal)
        {
            return teacherRepository.listStudentCalendarsRepository(curseItem, countCal);
        }

        public void confirmAssistanceService(int[] assistance, User user, int curseItem, int countCal)
        {
            teacherRepository.deleteStudentAssistanceRepository(curseItem,countCal);
            teacherRepository.confirmAssistanceRepository(assistance, user, curseItem, countCal);
        }

        public TeacherCalendar teacherCalendarsClassService(int id)
        {
            return teacherRepository.teacherCalendarsClassRepository(id);
        }

    }
}