﻿using BackOffice.Repository;
using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Service
{
    public class TechnologyService
    {
        private TechnologyRepository technologyRepository = new TechnologyRepository();

        public List<Technology> listTechnologyService()
        {
            return technologyRepository.listTechnologyRepository();
        }

        public void createRepositoryService(string name, int active, int id)
        {

            this.technologyRepository.createRepositoryRepository(name,active, id);
        }

        public Technology findTechnologyService(int id)
        {
            return this.technologyRepository.findTechnologyRepository(id);
        }

        public void updateTechnologyService(int id, string name, int active)
        {
            this.technologyRepository.updateTechnologyRepository(id, name,active);
        }

        public ValidateTechnology validateTechnologyService(string name,int active,int idTec = 0,bool update=false) {

            ValidateTechnology validateTechnology = new ValidateTechnology();
            validateTechnology.countError = 0;

            if (name == "" || name == null )
            {
                validateTechnology.nameError = "El nombre es requerido";
                validateTechnology.countError+=1;
            }
            else if (null != technologyRepository.findTechnologyNameRepository(name, idTec))
            {
                validateTechnology.nameError = "El nombre de las tecnologias no pueden repetirse";
                validateTechnology.countError += 1;   
            }
          
            return validateTechnology;
        }

        public List<Technology> filterTechnologyActiveService(int active)
        {
            return this.technologyRepository.filterTechnologyActiveRepository(active);
        }
    }
}