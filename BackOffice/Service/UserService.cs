﻿using BackOffice.Repository;
using BackOffice.Validate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Service
{
    public class UserService
    {
        private UserRepository userRepository = new UserRepository();

        public List<User> listUser()
        {
            return userRepository.listUserRepository();
        }

        public void createUserService(int rol,string firstName, string lastName, string email, string password, string repeatPassword, int active, int? teacherPerson)
        {
            this.userRepository.createUserRepository(rol,firstName, lastName, email, password, repeatPassword, active, teacherPerson);
        }

        public User findUserService(int id)
        {
            return this.userRepository.findUserIdRepository(id);
        }

        public void uspdateUserService(int rol, int idUser, string firstName, string lastName, string email, string password, string repeatPassword, int active,int? teacherPerson)
        {
            this.userRepository.updateUserRepository(rol, idUser, firstName, lastName, email, password, repeatPassword, active, teacherPerson);
        }

        public User signInService(string email, string password)
        {
            return userRepository.signInRepository(email, password);
        }

        public ValidateLogin validateLoginService(string email,string password)
        {
            ValidateLogin validateLogin = new ValidateLogin();
            validateLogin.countError = 0;
            User user = userRepository.signInRepository(email, password);
            if (user == null)
            {
                validateLogin.loginError = "El email o el password son incorrectos";
                validateLogin.countError += 1;
            }
            return validateLogin;
        }
        
        public ValidateUser ValidateUserService(string firstName, string lastName, string email,int active, string password, string repeatPassword, int rol, int? teacherPerson , bool update = false, int? idUser = null)
        {
            ValidateUser validateUser = new ValidateUser();
            validateUser.countError = 0;

            //nombre
            if (firstName == "")
            {
                validateUser.firstNameError = "El nombre es requerido";
                validateUser.countError +=1;
            }
            else if (firstName.Length > 200)
            {
                validateUser.firstNameError = "El nombre no puede exeder los 200 caracteres";
                validateUser.countError += 1;
            }

            //apellido
            if (lastName == "")
            {
                validateUser.lastNameError = "El apellido es requerido";
                validateUser.countError += 1;   
            }
            else if (lastName.Length > 200)
            {
                validateUser.lastNameError = "El apellido no puede exeder los 200 caracteres";
                validateUser.countError += 1;
            }

            //email
            User user = userRepository.finUserEmailRepository(email);
            if (email == "")
            {
                validateUser.emailError = "El email es requerido";
                validateUser.countError += 1;
            }
            else if (email.Length > 150)
            {
                validateUser.emailError = "El email no puede exeder los 150 caracteres";
                validateUser.countError += 1;
            }
            else if (null != user)
            {
                if (user.Id != idUser) {
                    validateUser.emailError = "El email ya esta requistrado con otro usuario";
                    validateUser.countError += 1;
                }
            }

            //Password
            if (password == "")
            {
                if (update == false)
                {
                    validateUser.passwordError = "El password es requerido";
                    validateUser.countError += 1;
                }
            }
            else if (password.Length > 250)
            {
                validateUser.passwordError = "El password no puede exeder los 200 caracteres";
                validateUser.countError += 1;
            }

            //repetir password
            if (repeatPassword != password)
            {
                validateUser.repeatPasswordError = "Los password no son iguales";
                validateUser.countError += 1;
            }

            //comprobar relacion usuario y profesor
            if (rol == 2 && teacherPerson == null)
            {
                validateUser.teacherPersonError = "No fue asignado un profesor para el rol de mentor";
                validateUser.countError += 1;
            }

            if (null != userRepository.linkedUserTeacher(teacherPerson, idUser))
            {
                validateUser.teacherPersonError = "Este mentor ya fue asignado a otro usuario";
                validateUser.countError += 1;
            }
            

            return validateUser;
        }

        public List<User> filterUserActiveService(int active)
        {
            return userRepository.filterUserActiveRepository(active);
        }
    }
}