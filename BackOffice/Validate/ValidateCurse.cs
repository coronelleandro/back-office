﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Validate
{
    public class ValidateCurse
    {
        public int countError { set; get; }

        public string nameError { set; get; }

        public string contentsTeachedError { set; get; }

        public string descriptionError { set; get; }

        public string requerimentsError { set; get; }

        public string linkPhotoError { set; get; }

        public string technologyError { set; get; }
    }
}