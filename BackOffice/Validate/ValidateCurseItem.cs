﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Validate
{
    public class ValidateCurseItem
    {
        public int countError { set; get; }

        public string dateStartError { set; get; }

        public string dateEndError { set; get; }

        public string timeStartError { set; get; }

        public string timeEndError { set; get; }

        public string studentsCapacityError { set; get; }

        public string priceError { set; get; }

        public string teacherError { set; get; }
        
        public string daysError { set; get; }

        public string studentError { set; get; }
    }
}