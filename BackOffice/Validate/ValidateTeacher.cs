﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Validate
{
    public class ValidateTeacher
    {
        public int countError { set; get; }

        public string firstNameError { set; get; }

        public string lastNameError { set; get; }

        public string emailError { set; get; }

        public string dniError { set; get; }

        public string telephoneError { set; get; }

        public string technologyError { set; get; }

        public string observationsError { set; get; }
    }
}