﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackOffice.Validate
{
    public class ValidateUser
    {
        public int countError { set; get; }

        public string firstNameError { set; get; }

        public string lastNameError { set; get; }

        public string emailError { set; get; }

        public string passwordError { set; get; }

        public string repeatPasswordError { set; get; }

        public string teacherPersonError { set; get; }
    }
}