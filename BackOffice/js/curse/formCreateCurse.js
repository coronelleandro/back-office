﻿var file = document.getElementById("file");
file.onchange = function (e) {
    // Creamos el objeto de la clase FileReader
    let reader = new FileReader();

    // Leemos el archivo subido y se lo pasamos a nuestro fileReader
    reader.readAsDataURL(e.target.files[0]);

    // Le decimos que cuando este listo ejecute el código interno
    reader.onload = function () {
        let preview = document.getElementById('preview'),
            image = document.createElement('img');
        image.style.width = "500px";
        image.style.height = "300px";
        image.className = "img-thumbnail";
        image.src = reader.result;

        preview.innerHTML = '';
        preview.append(image);
    };
};


//validar curse
let createCurse = document.getElementById("createCurse");
createCurse.addEventListener("submit", validateCurse)

function validateCurse() {
    var evento = window.event || e;
    evento.preventDefault();

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                //filterTeacher.innerHTML = xmlhttp.responseText;
                let response = JSON.parse(xmlhttp.responseText)
                confirmValidate(response)

            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    xmlhttp.open("POST", "/Curse/validateCurse", true);

    var data = new FormData();
    data.append("name", document.getElementById("name").value);
    data.append("idTec", document.getElementById("idTec").value);
    data.append("content", document.getElementById("content").value);
    data.append("description", document.getElementById("description").value);
    data.append("requirements", document.getElementById("requirements").value);
    data.append("file", document.getElementById("file").value);  
    data.append("active", document.getElementById("active").value);
    xmlhttp.send(data);
}

function confirmValidate(response) {
    console.log(response)
    if (response.countError != 0) {
        document.getElementById("nameError").innerHTML = response.nameError
        document.getElementById("technologyError").innerHTML = response.technologyError
        document.getElementById("contentsTeachedError").innerHTML = response.contentsTeachedError
        document.getElementById("descriptionError").innerHTML = response.descriptionError
        document.getElementById("requerimentsError").innerHTML = response.requerimentsError
        document.getElementById("linkPhotoError").innerHTML = response.linkPhotoError
    }
    else {
        document.getElementById("createCurse").submit()
    }
}