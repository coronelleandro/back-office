﻿let filter = document.getElementById("filter");

filter.addEventListener("click", function () {

    let filterCurseItems = document.getElementById("filterCurseItems");


    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                filterCurseItems.innerHTML = xmlhttp.responseText;

                //console.log(xmlhttp.responseText)
            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    //string firsName, string lastName, string dni
    xmlhttp.open("POST", "/curseItem/filterCurseItem", true);

    var data = new FormData();
    data.append("active", document.getElementById("active").value);
    data.append("idTec", document.getElementById("idTec").value);
    xmlhttp.send(data);

})