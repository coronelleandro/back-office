﻿//validar curse
let createCurseItem = document.getElementById("createCurseItem");
createCurseItem.addEventListener("submit", validateCurse)

function validateCurse() {
    var evento = window.event || e;
    evento.preventDefault();

    console.log(document.getElementById("idTeacher").value);
    
    let days = document.getElementsByName("days")
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {

                //console.log(JSON.parse(xmlhttp.responseText))
                let response = JSON.parse(xmlhttp.responseText)
                confirmValidate(response)
            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    xmlhttp.open("POST", "/CurseItem/validateCurseItem", true);

    var data = new FormData();
    data.append("idCurse", document.getElementById("idCurse").value);
    data.append("idTeacher", document.getElementById("idTeacher").value);
    data.append("dateStart", document.getElementById("dateStart").value);
    data.append("dateFinish", document.getElementById("dateFinish").value);
    data.append("timeStart", document.getElementById("timeStart").value);
    data.append("timeFinish", document.getElementById("timeFinish").value);
    data.append("state", document.getElementById("state").value);
    data.append("active", document.getElementById("active").value);
    for (let i = 0; i < days.length; i++) {
        if (days[i].checked) {
            data.append("days", days[i].value)
        }
    }
    data.append("studentsCapacity", document.getElementById("studentsCapacity").value);
    data.append("price", document.getElementById("price").value);
    xmlhttp.send(data);
}

function confirmValidate(response) {
    console.log(response)
    if (response.countError != 0) {
        document.getElementById("teacherError").innerHTML = response.teacherError
        document.getElementById("dateStartError").innerHTML = response.dateStartError
        document.getElementById("dateEndError").innerHTML = response.dateEndError
        document.getElementById("timeStartError").innerHTML = response.timeStartError
        document.getElementById("timeEndError").innerHTML = response.timeEndError
        document.getElementById("daysError").innerHTML = response.daysError
        document.getElementById("studentsCapacityError").innerHTML = response.timeEndError
        document.getElementById("priceError").innerHTML = response.priceError
    }
    else {
        document.getElementById("createCurseItem").submit()
    }
}