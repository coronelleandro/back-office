﻿//tab 
let common = document.getElementById("common");
let students = document.getElementById("students");

common.style.visibility = "visible";
common.style.position = "relative";

students.style.visibility = "hidden";
students.style.position = "absolute";

let commonSectionTab = document.getElementById("commonSection-tab");
commonSectionTab.addEventListener("click", function (e) {

    var evento = window.event || e;
    evento.preventDefault();

    common.style.visibility = "visible";
    common.style.position = "relative";

    students.style.visibility = "hidden";
    students.style.position = "absolute";

    commonSectionTab.className = "nav-item nav-link active";
    studentSectionTab.className = "nav-item nav-link";
})

let studentSectionTab = document.getElementById("studentSection-tab");
studentSectionTab.addEventListener("click", function (e) {

    var evento = window.event || e;
    evento.preventDefault();

    students.style.visibility = "visible";
    students.style.position = "relative";

    common.style.visibility = "hidden";
    common.style.position = "absolute";

    commonSectionTab.className = "nav-item nav-link";
    studentSectionTab.className = "nav-item nav-link active";
})


//validate curse
let updateCurseItem = document.getElementById("updateCurseItem");
updateCurseItem.addEventListener("submit", validateCurseItem)

function validateCurseItem() {
    var evento = window.event || e;
    evento.preventDefault();

    let idStudents = document.getElementsByName("idStudents")
 
    let days = document.getElementsByName("days")

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {

                let response = JSON.parse(xmlhttp.responseText)
                confirmValidate(response)
            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    xmlhttp.open("POST", "/CurseItem/updateValidateCurseItem", true);

    var data = new FormData();
    data.append("idCurseItem", document.getElementById("idCurseItem").value)
    data.append("idCurse", document.getElementById("idCurse").value);
    data.append("idTeacher", document.getElementById("idTeacher").value);
    data.append("dateStart", document.getElementById("dateStart").value);
    data.append("dateFinish", document.getElementById("dateFinish").value);
    data.append("timeStart", document.getElementById("timeStart").value);
    data.append("timeFinish", document.getElementById("timeFinish").value);
    data.append("state", document.getElementById("state").value);
    data.append("active", document.getElementById("active").value);
    for (let i = 0; i < days.length; i++) {
        if (days[i].checked) {
            data.append("days", days[i].value)
        }
    }
    data.append("studentsCapacity", document.getElementById("studentsCapacity").value);
    data.append("price", document.getElementById("price").value);
    for (let i = 0; i < idStudents.length; i++) {
        console.log(idStudents[i].value)
        data.append("idStudents", idStudents[i].value);
    }
    xmlhttp.send(data);
}

function confirmValidate(response) {
    console.log(response)
    if (response.countError != 0) {
        document.getElementById("teacherError").innerHTML = response.teacherError
        document.getElementById("dateStartError").innerHTML = response.dateStartError
        document.getElementById("dateEndError").innerHTML = response.dateEndError
        document.getElementById("timeStartError").innerHTML = response.timeStartError
        document.getElementById("timeEndError").innerHTML = response.timeEndError
        document.getElementById("daysError").innerHTML = response.daysError
        document.getElementById("studentsCapacityError").innerHTML = response.studentsCapacityError
        document.getElementById("priceError").innerHTML = response.priceError
    }
    else {
        document.getElementById("updateCurseItem").submit()
    }
}


//ajax
let studentName = document.getElementById("studentName");
let listStudent = document.getElementById("listStudent");

//add
let addOneStudent = document.getElementById("addOneStudent");
let addStudent = document.getElementById("addStudent");
let studentInput = document.getElementsByClassName("studentInput");
console.log(studentInput.length)

listStudent.addEventListener("click",function (e) {
    //console.log(e.target)
    let valide = true
    if (e.target.name == "addOneStudent") {
        if (studentInput != null)
        {
            for (var i = 0; i < studentInput.length; i++)
            {
                //console.log("stuentInput " + studentInput[i].value)
                //console.log("value " + e.target.parentElement.children[0].value)
                if (studentInput[i].value == e.target.parentElement.children[0].value)
                {
                    //console.log(e.target.parentElement.children[0].value)
                    valide = false
                }
            }
        }

        if (valide == true) {
            let contentValue = e.target.parentElement.children[0].value
            let contentText = e.target.parentElement.children[1].innerText
            ///console.log(e.target.parentElement.children[1].innerText)
            //console.log("<div><input type='text' value='" + contentValue + "'>" + contentText + "</div>")
            addStudent.innerHTML += "<div class='col-6  p-2 mt-1 d-flex justify-content-between'><input class='studentInput' name='idStudents' type='hidden' value='" + contentValue + "'>" + contentText + "<button type='button' name='deleteStudent' class='deleteStudent btn btn-danger'>Eliminar</button></div>"
            studentInput = document.getElementsByClassName("studentInput")
        }
        //addOneStudent = ''
    }
})

addStudent.addEventListener("click", function (e) {
    if (e.target.name == "deleteStudent") {
        //console.log(e.target.parentElement)
        e.target.parentElement.remove()
    }
})

studentName.addEventListener("keyup", function () {
    //console.log(student.value);
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                listStudent.innerHTML = xmlhttp.responseText;
                
                //selectStudent(); 
            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
  
    xmlhttp.open("POST", "/curseItem/findStudentCurseItem", true);
    //alert(student.value);
    var data = new FormData();
    data.append("studentName", studentName.value);
    xmlhttp.send(data);
})


