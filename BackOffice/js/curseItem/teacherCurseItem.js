﻿let idCurse = document.getElementById("idCurse");
let idTeacher = document.getElementById("idTeacher");

findTeacher();

idCurse.addEventListener("change", findTeacher)

function findTeacher() {

    idTeacher.innerHTML = '<option value="0" selected>No hay Profesor</option>';
    console.log(idTeacher.innerHTML);
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                idTeacher.innerHTML = xmlhttp.responseText;
            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };

    xmlhttp.open("POST", "/curseItem/teacherCurseItem", true);
    //alert("holaa")
    var data = new FormData();
    data.append("idCurse", idCurse.value);
    xmlhttp.send(data);
}
