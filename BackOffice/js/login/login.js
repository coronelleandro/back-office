﻿let signIn = document.getElementById("signIn")
signIn.addEventListener("submit", validateLogin)

function validateLogin() {
    var evento = window.event || e;
    evento.preventDefault();

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {

                response = JSON.parse(xmlhttp.responseText)
                confirmValidate(response)

            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    xmlhttp.open("POST", "/Login/validateLogin", true);

    var data = new FormData();
    data.append("email", document.getElementById("email").value);
    data.append("password", document.getElementById("password").value);
    xmlhttp.send(data);
}

function confirmValidate(response) {
    console.log(response)
    if (response.countError != 0) {
        document.getElementById("loginError").innerHTML = response.loginError
    }
    else {
        document.getElementById("signIn").submit()
    }
}