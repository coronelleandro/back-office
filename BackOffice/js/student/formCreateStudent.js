﻿var file = document.getElementById("file");
file.onchange = function (e) {
    // Creamos el objeto de la clase FileReader
    let reader = new FileReader();

    // Leemos el archivo subido y se lo pasamos a nuestro fileReader
    reader.readAsDataURL(e.target.files[0]);

    // Le decimos que cuando este listo ejecute el código interno
    reader.onload = function () {
        let preview = document.getElementById('preview'),
            image = document.createElement('img');
        image.style.width = "500px";
        image.style.height = "300px";
        image.src = reader.result;
        image.className = "img-thumbnail";

        preview.innerHTML = '';
        preview.append(image);
    };
};

//validar usuario
let createStudent = document.getElementById("createStudent")
createStudent.addEventListener("submit", validateUser)

function validateUser() {
    var evento = window.event || e;
    evento.preventDefault();

    //console.log(document.getElementById("teacherPerson").value)
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {

                let response = JSON.parse(xmlhttp.responseText)
                confirmValidate(response) 

            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    xmlhttp.open("POST", "/Student/validateStudent", true);

    var data = new FormData();
    data.append("firstName", document.getElementById("firstName").value);
    data.append("lastName", document.getElementById("lastName").value);
    data.append("email", document.getElementById("email").value);
    data.append("dni", document.getElementById("dni").value);
    data.append("telephone", document.getElementById("telephone").value);
    data.append("file", document.getElementById("file").value);
    data.append("observations", document.getElementById("observations").value);
    data.append("active", document.getElementById("active").value);
    xmlhttp.send(data);
}

function confirmValidate(response) {
    console.log(response)
    if (response.countError != 0) {
        document.getElementById("firstNameError").innerHTML = response.firstNameError
        document.getElementById("lastNameError").innerHTML = response.lastNameError
        document.getElementById("emailError").innerHTML = response.emailError
        document.getElementById("dniError").innerHTML = response.dniError
        document.getElementById("telephoneError").innerHTML = response.telephoneError
    }
    else {
        document.getElementById("createStudent").submit()
    }
}