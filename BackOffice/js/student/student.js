﻿let filter = document.getElementById("filter");
filter.addEventListener("click", function () {
    let active = document.getElementById("active").value;
    //console.log(active);
    let firstName = document.getElementById("firstName").value;

    let lastName = document.getElementById("lastName").value;

    let dni = document.getElementById("dni").value;

    let studentFilter = document.getElementById("studentFilter");
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                studentFilter.innerHTML = xmlhttp.responseText;
                //console.log(xmlhttp.responseText)
            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    //string firsName, string lastName, string dni
    xmlhttp.open("GET", "/Student/filterStudent?active=" + active + "&firstName=" + firstName + "&lastName=" + lastName + "&dni=" + dni, true);
    xmlhttp.send();
})