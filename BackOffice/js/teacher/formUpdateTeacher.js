﻿//validar teacher
let updateTeacher = document.getElementById("updateTeacher");
updateTeacher.addEventListener("submit", validateTeacher)

function validateTeacher() {
    var evento = window.event || e;
    evento.preventDefault();
    console.log("tec" + document.getElementById("idTec").value)

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                //filterTeacher.innerHTML = xmlhttp.responseText;
                response = JSON.parse(xmlhttp.responseText)
                confirmValidate(response)

            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    xmlhttp.open("POST", "/Teacher/updateValidarTeacher", true);

    var data = new FormData();
    data.append("firstName", document.getElementById("firstName").value);
    data.append("lastName", document.getElementById("lastName").value);
    data.append("email", document.getElementById("email").value);
    data.append("dni", document.getElementById("dni").value);
    data.append("telephone", document.getElementById("telephone").value);
    data.append("linkedin", document.getElementById("linkedin").value);
    data.append("obserbations", document.getElementById("obserbations").value);
    /*for (let i = 0; i<) {
        data.append("idTec", document.getElementById("idTec").value);
    }*/
    data.append("active", document.getElementById("active").value);
    xmlhttp.send(data);
}

function confirmValidate(response) {
    console.log(response)
    if (response.countError != 0) {
        document.getElementById("firstNameError").innerHTML = response.firstNameError
        document.getElementById("lastNameError").innerHTML = response.lastNameError
        document.getElementById("emailError").innerHTML = response.emailError
        document.getElementById("dniError").innerHTML = response.dniError
        document.getElementById("telephoneError").innerHTML = response.telephoneError
    }
    else {
        document.getElementById("updateTeacher").submit()
    }
}

//agregar tecnologia
let addTecnologia = document.getElementById("addTecnologia")
let idTec = document.getElementById("idTec")
let array = new Array();
let tecnoligies = document.getElementById("tecnoligies");

addTecnologia.addEventListener("click", addOneTechnology)

let inputValue;

function addOneTechnology() {
    let nameTec = idTec.options[idTec.selectedIndex].text

    let validate = true;
    if (inputValue != null) {
        for (let i = 0; i < inputValue.length; i++) {
            if (idTec.value == inputValue[i].value) {
                console.log("son iguales")
                validate = false;
            }
        }
    }

    if (validate == true) {
        tecnoligies.innerHTML += "<div class='idtechnology col-6  p-1 mt-1 d-flex justify-content-between'><input class='inputValue' name='idTechnoloy[]' type='hidden' checked value='" + idTec.value + "'>" + nameTec + "<button type='button' name='deleteTec' class='btn btn-danger deleteTec'>Eliminar</button></div>"
    }
    inputValue = document.getElementsByClassName("inputValue")
    idtechnology = document.getElementsByClassName("idtechnology")
    deleteTec = document.getElementsByClassName("deleteTec")

}

tecnoligies.addEventListener("click", function (e) {
    //console.log(e.target)
    if (e.target.name == 'deleteTec') {
        console.log(e.target.parentElement)
        e.target.parentElement.remove()
    }
})

//imagenes
var file = document.getElementById("file");

file.onchange = function (e) {
    let reader = new FileReader();

    reader.readAsDataURL(e.target.files[0]);

    reader.onload = function () {
        let preview = document.getElementById('preview'),
            image = document.createElement('img');
        image.style.width = "500px";
        image.style.height = "300px";
        image.className = "img-thumbnail";
        image.src = reader.result;

        preview.innerHTML = '';
        preview.append(image);
    };
};


