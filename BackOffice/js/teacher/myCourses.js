﻿let curseItem = document.getElementById("curseItem");
let teacherCalendar = document.getElementById("teacherCalendar");

myCoursesCalendar();

curseItem.addEventListener("change", myCoursesCalendar)

function myCoursesCalendar() {
        
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                //console.log(xmlhttp.responseText)
                teacherCalendar.innerHTML = xmlhttp.responseText;
                //console.log(new Date(date[0].dateHourEnd))
            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    
    xmlhttp.open("POST", "/Teacher/myCoursesCalendar", true);

    var data = new FormData();
    data.append("curseItem", curseItem.value);
    xmlhttp.send(data);
   
}