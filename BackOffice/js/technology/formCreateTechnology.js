﻿let createTechnology = document.getElementById("createTechnology")

createTechnology.addEventListener("submit", validateTechnology)

function validateTechnology() {
    var evento = window.event || e;
    evento.preventDefault();

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {

                response = JSON.parse(xmlhttp.responseText)
                confirmValidate(response)

            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    xmlhttp.open("POST", "/Technology/validarTechnology", true);

    var data = new FormData();
    data.append("name", document.getElementById("name").value);
    data.append("active", document.getElementById("active").value);
    xmlhttp.send(data);
}

function confirmValidate(response) {
    console.log(response)
    if (response.countError != 0) {
        document.getElementById("nameError").innerHTML = response.nameError
    }
    else {
        document.getElementById("createTechnology").submit()
    }
}