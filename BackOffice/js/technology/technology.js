﻿let filter = document.getElementById("filter");
filter.addEventListener("click", function () {
    let active = document.getElementById("active").value;
    //console.log(active);
    let filterTechnology = document.getElementById("filterTechnology");
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                filterTechnology.innerHTML = xmlhttp.responseText;
                //console.log(xmlhttp.responseText)
            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    xmlhttp.open("GET", "/Technology/filterTechnology?active=" + active, true);
    xmlhttp.send();
    
})