﻿let rol = document.getElementById("rol");

let teacherDiv = document.getElementById("teacherDiv");
teacherDiv.style.display = "none";

rol.addEventListener("change", selectRol)

let teacherPerson = null

function selectRol() {
    if (rol.value == 2) {
        teacherDiv.style.display = "block";
        teacherPerson = 1
        document.getElementById("teacherPerson").setAttribute("name", "teacherPerson")
        //console.log(document.getElementById("teacherPerson").getAttribute("name"))
    }
    else {
        teacherDiv.style.display = "none";
        teacherPerson = 2
        document.getElementById("teacherPerson").setAttribute("name", null)
        //console.log(document.getElementById("teacherPerson").getAttribute("name"))
    }
}

//validar usuario
let createUser = document.getElementById("createUser")
createUser.addEventListener("submit", validateUser)

function validateUser() {
    var evento = window.event || e;
    evento.preventDefault();

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {

                response = JSON.parse(xmlhttp.responseText)
                confirmValidate(response)

            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    xmlhttp.open("POST", "/User/validateUser", true);

    var data = new FormData();
    data.append("rol", document.getElementById("rol").value);
    data.append("firstName", document.getElementById("firstName").value);
    data.append("lastName", document.getElementById("lastName").value);
    data.append("email", document.getElementById("email").value);
    data.append("password", document.getElementById("password").value);
    data.append("repeatPassword", document.getElementById("repeatPassword").value);
    if (teacherPerson == 1) {
        data.append("teacherPerson", document.getElementById("teacherPerson").value)
    }
    else {
        data.append("teacherPerson", null)
    }
    data.append("active", document.getElementById("active").value);
    xmlhttp.send(data);
}

function confirmValidate(response) {
    console.log(response)
    if (response.countError != 0) {
        document.getElementById("firstNameError").innerHTML = response.firstNameError
        document.getElementById("lastNameError").innerHTML = response.lastNameError
        document.getElementById("emailError").innerHTML = response.emailError
        document.getElementById("passwordError").innerHTML = response.passwordError
        document.getElementById("repeatPasswordError").innerHTML = response.repeatPasswordError
        document.getElementById("teacherPersonError").innerHTML = response.teacherPersonError
    }
    else {
        document.getElementById("createUser").submit()
    }
}
