﻿let filter = document.getElementById("filter");
filter.addEventListener("click", function () {
    let active = document.getElementById("active").value;
    //console.log(active);
    let filterUser = document.getElementById("filterUser");
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                filterUser.innerHTML = xmlhttp.responseText;

            }
            else if (xmlhttp.status == 400) {
                console.log('There was an error 400');
            }
            else {
                console.log('something else other than 200 was returned');
            }
        }
    };
    xmlhttp.open("GET", "/User/filterUser?active=" + active, true);
    xmlhttp.send();
    
})